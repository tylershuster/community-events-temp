// import '../stickyfill.js';
/*
*     __           __   __   ___      ___  ___  __
*    /  \ |\ |    /  ` |__) |__   /\   |  |__  |  \
*    \__/ | \|    \__, |  \ |___ /~~\  |  |___ |__/
*
*/
Template.home.onCreated(function () {
	Session.set('calendarView', true);
	var view = 'calendar';
	if(localStorage && localStorage.calendarViewType) view = localStorage.calendarViewType;
	this.view = new ReactiveVar(view);
	this.categoryFilter = new ReactiveVar('all');
});

/*
*     __           __   ___       __   ___  __   ___  __
*    /  \ |\ |    |__) |__  |\ | |  \ |__  |__) |__  |  \
*    \__/ | \|    |  \ |___ | \| |__/ |___ |  \ |___ |__/
*
*/
// Template.header.onRendered(function () {
// 	$('header').fixedsticky();
// });

/*
*     __           __   ___  __  ___  __   __       ___  __
*    /  \ |\ |    |  \ |__  /__`  |  |__) /  \ \ / |__  |  \
*    \__/ | \|    |__/ |___ .__/  |  |  \ \__/  |  |___ |__/
*
*/

/*
*     ___       ___      ___  __
*    |__  \  / |__  |\ |  |  /__`
*    |___  \/  |___ | \|  |  .__/
*
*/
Template.header.events({
	'click #login_register' (event, template) {
		event.preventDefault();
		Meteor.loginWithGoogle();
		// Blaze.render(Template.login_register, event.target.parentElement);
	},
	'click .calendar--next' (event, template) {
		var currentSpan;
		if ($('#events-calendar').length) {
			$('#events-calendar').fullCalendar('next');
			currentSpan = $('#events-calendar').fullCalendar('getDate').format('MMMM');
		} else {
			var listTemplate = Blaze.getView(document.getElementById('event__list'));
			currentSpan = months[months.indexOf(Session.get('calendarSpan')) + 1];
			listTemplate._templateInstance.jumpTo(currentSpan);
		}
		// Session.set('calendarSpan', currentSpan);
	},
	'click .calendar--prev' (event, template) {
		var currentSpan;
		if ($('#events-calendar').length) {
			$('#events-calendar').fullCalendar('prev');
			currentSpan = $('#events-calendar').fullCalendar('getDate').format('MMMM');
		} else {
			var listTemplate = Blaze.getView(document.getElementById('event__list'));
			currentSpan = months[months.indexOf(Session.get('calendarSpan')) - 1];
			listTemplate._templateInstance.jumpTo(currentSpan);
		}
		// Session.set('calendarSpan', currentSpan);
	},
	'click #history--back' (event, template) {
		var history = Session.get('history');
		history.splice(history.length - 1, 1);
		Session.set('history', history);
	},
	'click #map--close' (event, template) {
		$('#map').removeClass('active');
		Session.set('mapOpen', false);
		window.scrollTo(0, 0);
	}
});

Template.home.events({
	'click #homeView span' (event, template) {
		// console.log(Blaze.g);
		if(localStorage) localStorage.calendarViewType = event.currentTarget.dataset.view;
		// var id = event.currentTarget.dataset.view === 'calendar' ? 'event__list' : 'events-calendar';
		// var view = Blaze.getView(document.getElementById(id));
		// $('#' + id).addClass('view--original');
		// $('#' + id).clone().addClass('leaving').removeClass('view--original').insertAfter('#' + id);
		// $('.view--original').remove();
		template.view.set(event.currentTarget.dataset.view);
		// Meteor.setTimeout(() => {
		// 	Blaze.remove(view);
		// 	$('#' + id).remove();
		// }, 1000);
	},
	'change [name=categoryFilter]' (event, template) {
		template.categoryFilter.set(event.target.value);
		var container;
		if(document.getElementById('event__list')) {
			container = Blaze.getView(document.getElementById('event__list')).templateInstance();
		} else if(document.getElementById('events-calendar')) {
			container = Blaze.getView(document.getElementById('events-calendar')).templateInstance();
		}
		container.category.set(event.target.value);
		if(container.renderHeaders) Meteor.setTimeout(container.renderHeaders, 1000);
	}
});

/*
*          ___       __   ___  __   __
*    |__| |__  |    |__) |__  |__) /__`
*    |  | |___ |___ |    |___ |  \ .__/
*
*/
Template.header.helpers({
	calendarView () {
		return Session.get('calendarView');
	},
	calendarSpan () {
		return Session.get('calendarSpan');
	},
	mapOpen () {
		return Session.get('mapOpen');
	}
});

Template.home.helpers({
	viewIs (type) {
		return Template.instance().view.get() === type ? 'active' : false;
	},
	category () {
		console.log('template', Template.instance());
		return Template.instance().categoryFilter.get();
	}
});



/*
*     ___            __  ___    __        __
*    |__  |  | |\ | /  `  |  | /  \ |\ | /__`
*    |    \__/ | \| \__,  |  | \__/ | \| .__/
*
*/
