Meteor.startup(() => {
	if(Meteor.isServer) {
		eventfulCategories = ApplicationDatum.findOne({name: 'Eventful Categories'});
		if(!eventfulCategories) {
			let url = 'http://api.eventful.com/json/categories/list';
			let params = {
				app_key: Meteor.settings.eventfulAPIKey,
			};
			HTTP.get(url, {params}, (error, response) => {
				if(!error) {
					var categories = JSON.parse(response.content).category;
					eventfulCategories = new ApplicationDatum();
					eventfulCategories.name = 'Eventful Categories';
					eventfulCategories.pairs = {};
					eventfulCategories.pairs.timestamp = new Date();
					if(categories) {
						categories.forEach(category => {
							switch(category.id) {
								case 'music': eventfulCategories.pairs.music = Categories.findOne({name: 'Music'})._id; break;
								case 'conference': eventfulCategories.pairs.conference = Categories.findOne({name: 'Business'})._id; break;
								case 'comedy': eventfulCategories.pairs.comedy = Categories.findOne({name: 'Arts'})._id; break;
								case 'learning_education': eventfulCategories.pairs.learning_education = Categories.findOne({name: 'Family & Education'})._id; break;
								case 'family_fun_kids': eventfulCategories.pairs.family_fun_kids = Categories.findOne({name: 'Family & Education'})._id; break;
								case 'festivals_parades': eventfulCategories.pairs.festivals_parades = Categories.findOne({name: 'Community'})._id; break;
								case 'movies_film': eventfulCategories.pairs.movies_film = Categories.findOne({name: 'Hobbies'})._id; break;
								case 'food': eventfulCategories.pairs.food = Categories.findOne({name: 'Food & Drink'})._id; break;
								case 'fundraisers': eventfulCategories.pairs.fundraisers = Categories.findOne({name: 'Charity & Causes'})._id; break;
								case 'art': eventfulCategories.pairs.art = Categories.findOne({name: 'Arts'})._id; break;
								case 'support': eventfulCategories.pairs.support = Categories.findOne({name: 'Charity & Causes'})._id; break;
								case 'holiday': eventfulCategories.pairs.holiday = Categories.findOne({name: 'Community'})._id; break;
								case 'books': eventfulCategories.pairs.books = Categories.findOne({name: 'Family & Education'})._id; break;
								case 'attractions': eventfulCategories.pairs.attractions = Categories.findOne({name: 'Community'})._id; break;
								case 'community': eventfulCategories.pairs.community = Categories.findOne({name: 'Community'})._id; break;
								case 'business': eventfulCategories.pairs.business = Categories.findOne({name: 'Business'})._id; break;
								case 'singles_social': eventfulCategories.pairs.singles_social = Categories.findOne({name: 'Other'})._id; break;
								case 'schools_alumni': eventfulCategories.pairs.schools_alumni = Categories.findOne({name: 'Family & Education'})._id; break;
								case 'clubs_associations': eventfulCategories.pairs.clubs_associations = Categories.findOne({name: 'Other'})._id; break;
								case 'outdoors_recreation': eventfulCategories.pairs.outdoors_recreation = Categories.findOne({name: 'Sports & Fitness'})._id; break;
								case 'performing_arts': eventfulCategories.pairs.performing_arts = Categories.findOne({name: 'Arts'})._id; break;
								case 'animals': eventfulCategories.pairs.animals = Categories.findOne({name: 'Other'})._id; break;
								case 'politics_activism': eventfulCategories.pairs.politics_activism = Categories.findOne({name: 'Community'})._id; break;
								case 'sales': eventfulCategories.pairs.sales = Categories.findOne({name: 'Business'})._id; break;
								case 'science': eventfulCategories.pairs.science = Categories.findOne({name: 'Family & Education'})._id; break;
								case 'religion_spirituality': eventfulCategories.pairs.religion_spirituality = Categories.findOne({name: 'Spirituality'})._id; break;
								case 'sports': eventfulCategories.pairs.sports = Categories.findOne({name: 'Sports & Fitness'})._id; break;
								case 'technology': eventfulCategories.pairs.technology = Categories.findOne({name: 'Hobbies'})._id; break;
								case 'other': eventfulCategories.pairs.other = Categories.findOne({name: 'Other'})._id; break;
							}
						});
					}
					eventfulCategories.save();
				} else {
					console.log(error);
				}
			})
		}
	}
	let eventfulCrawler = options => {
		if(Meteor.isServer) {
			let getEventfulEvents = options => {
				let url = 'http://api.eventful.com/json/events/search';
				let params = {
					app_key: Meteor.settings.eventfulAPIKey,
					location: 'Redding, CA',
					include: 'categories,price'
				};
				HTTP.get(url, {params}, (error, response) => {
					if(!error) {
						var events = JSON.parse(response.content).events;
						if(events) {
							events.event.forEach(eventfulEvent => {
								// console.log(eventfulEvent);
								var eventExists = Event.findOne({'remote.id': eventfulEvent.id});
								var event;
								if( eventExists ) {
									event = eventExists;
									event._isNew = false;
								} else {
									event = new Event();
									event._isNew = true;
									event.remote = new EventRemote();
									event.remote.name = 'eventful';
									event.remote.id = eventfulEvent.id;
								}
								event.title = eventfulEvent.title;
								event.description = eventfulEvent.description || '';
								event.duration = event.duration || {};
								event.duration.allDay = event.duration.allDay || eventfulEvent.all_day === '1';
								event.duration.start = event.duration.start || new Date(eventfulEvent.start_time);
								event.duration.end = event.duration.end || new Date(eventfulEvent.stop_time);
								event.location = event.location || {};
								event.location.name = event.location.name || eventfulEvent.venue_name;
								event.location.city = event.location.city || eventfulEvent.city_name;
								event.location.country = event.location.country || eventfulEvent.country_abbr2;
								event.location.lngLat = new LngLat();
								event.location.lngLat.lat = event.location.lngLat.lat || Number(eventfulEvent.latitude);
								event.location.lngLat.lng = event.location.lngLat.lng || Number(eventfulEvent.longitude);
								event.location.state = event.location.state || eventfulEvent.region_abbr;
								event.location.street = event.location.street || eventfulEvent.venue_address;
								event.location.zip = event.location.zip || eventfulEvent.postal_code;
								event.categoryIds = event.categoryIds || [];
								eventfulEvent.categories.category.forEach((category) => {
									if(event.categoryIds.indexOf(category.id) < 0) event.categoryIds.push(eventfulCategories.pairs[category.id]);
								});
								event.cost = event.cost || eventfulEvent.price;
								event.ageLimit = event.ageLimit || '';
								event.owner = event.owner || '';
								event.image = event.image || {};
								event.attendees = event.attendees || [];
								event.save((error, result) => {
									// console.log(error, result);
								});
							});
						}
					} else {
						console.log(error);
					}
				});
			};
			return getEventfulEvents(options);
		}
	};
	eventful = new EventSource({
		name: 'eventful',
		crawler: eventfulCrawler,
		linkResolver: remote => 'https://eventful.com/' + remote.id
	});
});
