import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
Clusterize = require('clusterize.js');

import '../../ui/layouts/app-body.html';
import '../../ui/layouts/app-body.js';

import '../../ui/components/event/view.html';
import '../../ui/components/event/view.js';

if(!Session.get('history')) {
	Session.set('history', []);
}

FlowRouter.route( '/', {
	name: 'home',
	action: function (params, queryParams) {
		// require('../../ui/pages/home.html');
		// let view = queryParams.view ? queryParams.view : 'List';
		BlazeLayout.render( 'layout', {header: 'header', main: 'home'});
		// BlazeLayout.render('eventList');
	}
});

FlowRouter.route('/login', {
	name: 'login',
	action: function (params, queryParams) {
		BlazeLayout.render('layout', {header: 'header', main: 'login_register'});
	}
});


FlowRouter.route('/subscriptions', {
	name: 'subscriptions',
	action: (params, queryParams) => {
		require('../../ui/pages/subscriptions.html');
		require('../../ui/pages/subscriptions.js');
		if(Meteor.user) {
			BlazeLayout.render('layout', {header: 'header', main: 'subscriptions'});
		} else {
			FlowRouter.go('/');
		}
	}
});

FlowRouter.route('/event/:eventId', {
	name: 'eventView',
	action: (params, queryParams) => {
		BlazeLayout.render('layout', {header: 'header', main: 'eventView'});
	}
});

FlowRouter.route('/event/:eventId/edit', {
	name: 'eventEdit',
	action: (params, queryParams) => {
		require('../../ui/components/event/edit.html');
		require('../../ui/components/event/edit.js');
		BlazeLayout.render('layout', {header: 'header', main: 'eventEdit'});
	}
});

FlowRouter.route('/sponsors', {
	name: 'sponsors',
	action: (params, queryParams) => {
		require('../../ui/pages/sponsor.html');
		require('../../ui/pages/sponsor.js');
		BlazeLayout.render('layout', {header: 'header', main: 'sponsor'});
	}
});

FlowRouter.route('/sponsor/category/:categoryId', {
	name: 'sponsorCategory',
	action: (params, queryParams) => {
		require('../../ui/pages/sponsor.html');
		require('../../ui/pages/sponsor.js');
		BlazeLayout.render('layout', {header: 'header', main: 'sponsorCategory'});
	}
});

FlowRouter.route('/sponsor/new/:categoryId?', {
	name: 'sponsorNew',
	action: (params, queryParams) => {
		require('../../ui/pages/sponsor.html');
		require('../../ui/pages/sponsor.js');
		BlazeLayout.render('layout', {header: 'header', main: 'sponsorNew'});
	}
});

// FlowRouter.route('/profile')
