/*
*     __           __   __   ___      ___  ___  __
*    /  \ |\ |    /  ` |__) |__   /\   |  |__  |  \
*    \__/ | \|    \__, |  \ |___ /~~\  |  |___ |__/
*
*/
Template.eventEdit.onCreated(function () {
	this.event = () => Event.findOne({_id: FlowRouter.getParam('eventId')});
});

/*
*     __           __   ___       __   ___  __   ___  __
*    /  \ |\ |    |__) |__  |\ | |  \ |__  |__) |__  |  \
*    \__/ | \|    |  \ |___ | \| |__/ |___ |  \ |___ |__/
*
*/
Template.eventEdit.onRendered(function () {
	initializePickers(this);
});
/*
*     __           __   ___  __  ___  __   __       ___  __
*    /  \ |\ |    |  \ |__  /__`  |  |__) /  \ \ / |__  |  \
*    \__/ | \|    |__/ |___ .__/  |  |  \ \__/  |  |___ |__/
*
*/

/*
*     ___       ___      ___  __
*    |__  \  / |__  |\ |  |  /__`
*    |___  \/  |___ | \|  |  .__/
*
*/
Template.eventEdit.events({
	'change input, change select, change textarea' (event, template) {
		console.log(event.target.value);
		var e = template.event();
		var value;
		switch(event.target.name) {
			case 'title':
				e.title = event.target.value;
				break;
			case 'description':
				e.description = event.target.value;
				break;
			case 'categoryIds':
				value = [];
				var i = 0;
				while (i < event.target.selectedOptions.length) {
					value.push(event.target.selectedOptions[i].value);
					i++;
				}
				e.categoryIds = value;
				break;
			case 'recurrence__exists':
				if(event.target.checked) {
					e.recurrence = new Recurrence();
					e.recurrence.duration = e.duration;
				} else {
					e.recurrence = undefined;
				}
				break;
			case 'recurrence__duration__allDay':
				e.recurrence.duration.allDay = event.target.checked ? true : false;
				break;
			case 'recurrence__duration__start':
				e.recurrence.duration.start = moment(event.target.value, 'h:mm A').toDate();
				break;
			case 'recurrence__duration__end':
				e.recurrence.duration.end = moment(event.target.value, 'h:mm A').toDate();
				break;
			case 'recurrence__dateRange__start':
				e.recurrence.dateRange = e.recurrence.dateRange || new DateRange();
				e.recurrence.dateRange.start = moment(event.target.value, 'MMM D YYYY').toDate();
				break;
			case 'recurrence__dateRange__end':
				e.recurrence.dateRange = e.recurrence.dateRange || new DateRange();
				e.recurrence.dateRange.end = moment(event.target.value, 'MMM D YYYY').toDate();
				break;
			case 'recurrence__frequency':
				e.recurrence.frequency = Number(event.target.value);
				break;
			case 'recurrence__interval':
				e.recurrence.interval = Number(event.target.value);
				break;
			case 'recurrence__count':
				e.recurrence.count = Number(event.target.value);
				break;
			case 'duration__allDay':
				e.duration.allDay = event.target.checked ? true : false;
				break;
			case 'duration__start--day':
				break;
			case 'duration__end--day':
				break;
			case 'duration__start--time':
				break;
			case 'duration__end--time':
				break;
			case 'location__street':
			case 'location__city':
			case 'location__state':
			case 'location__zip':
			case 'location__country':
				var field = event.target.name.replace('location__', '');
				e.location[field] = event.target.value;
				break;
			case 'location__lat':
				break;
			case 'location__lng':
				break;
			case 'cost':
				e.cost = event.target.value;
				break;
			case 'ageLimit':
				e.ageLimit = event.target.value;
				break;
			case 'image':
				FS.Utility.eachFile(event, file => {
					Images.insert(file, (err, fileObj) => {
						e.image = fileObj;
						e.save((error, result) => {
							console.log(error, result);
						});
					});
				});
				break;
			case 'remote':
				break;
			case 'delete':
				break;
			default:
				break;
		}
		console.log(e);
		e.save((error, id) => {
			console.log('saving', error, id);
			initializePickers(template);
		});
	},
	'click [name=delete]' (event, template) {
		var e = template.event();
		if (window.prompt('Are you sure you want to delete ' + e.title + '? Type yes to confirm.') === 'yes') {
			e.remove((error, result) => {
				if (!error) {
					FlowRouter.go('/');
				}
			});
		}
	},
	'click [name=duplicate]' (event, template) {
		var data = template.event();
		var parentNode = document.body;
		Blaze.renderWithData(Template.modalDuplicateDisambiguate, data, parentNode);
	}
});

Template.modalDuplicateDisambiguate.events({
	'submit #duplicateDisambiguate' (event, template) {
		event.preventDefault();
		var e = Event.findOne({_id: template.find('[name=event]').value});
		if(e) {
			e.mergeWith(template.data);
			$('#duplicateDisambiguate').remove();
			FlowRouter.go(`/event/${e._id}/edit`);
		}
	}
});
/*
*          ___       __   ___  __   __
*    |__| |__  |    |__) |__  |__) /__`
*    |  | |___ |___ |    |___ |  \ .__/
*
*/
Template.eventEdit.helpers({
	event () {
		return Template.instance().event();
	}
});

Template.modalDuplicateDisambiguate.helpers({
	events () {
		return Event.find();
	}
});


/*
*     ___            __  ___    __        __
*    |__  |  | |\ | /  `  |  | /  \ |\ | /__`
*    |    \__/ | \| \__,  |  | \__/ | \| .__/
*
*/
initializePickers = function (template) {
	Meteor.setTimeout(() => {
		var datepickers = [
			'[name=duration__start]',
			'[name=duration__end]',
			'[name=duration__start--day]',
			'[name=duration__end--day]',
			'[name=duration__start--day]',
			'[name=recurrence__dateRange__start]',
			'[name=recurrence__dateRange__end]'
		];
		var timepickers = [
			'[name=recurrence__duration__start]',
			'[name=recurrence__duration__end]',
			'[name=duration__start--time]',
			'[name=duration__end--time]',
		];
		template.$(datepickers.join()).pickadate({format: 'mmm d yyyy'});
		template.$(timepickers.join()).pickatime();
	}, 500);
};
