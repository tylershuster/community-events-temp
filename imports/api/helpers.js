parseUri = function (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};

Images = new FS.Collection( 'images', {
	stores: [
		new FS.Store.FileSystem( 'thumbnails', {
			transformWrite: (fileObj, readStream, writeStream) => {
				gm( readStream, fileObj.name() ).resize( '50', '50' ).stream().pipe( writeStream );
			}
		}),
		new FS.Store.FileSystem( 'images' ),
	],
	filter: {
		allow: {
			contentTypes: ['image/*', 'binary/octet-stream']
		}
	}
});

Images.deny({
	insert: () => false,
	update: () => false,
	remove: () => false,
	download: () => false
});

Images.allow({
	insert: () => true,
	update: () => true,
	remove: () => true,
	download: () => true,
});

if( Meteor.isServer ) {
	Meteor.publish( 'images', () => Images.find());
}

ApplicationData = new Mongo.Collection('applicationData');
ApplicationDatum = Astro.Class.create({
  name: 'Application Datum',
  collection: ApplicationData,
  fields: {
    name: String,
    pairs: Object
  }
});
