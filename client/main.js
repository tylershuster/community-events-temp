Template.registerHelper('categories', () => Category.find());

Template.registerHelper('last', array => array[array.length - 1]);

Template.registerHelper('history', () => {
	var history = Session.get('history');
	if(!history.length && FlowRouter.getRouteName() !== 'home') history = ['/'];
	return history;
});

Template.registerHelper('random', (low, high) => {
	return Math.floor(Math.random() * low) + high;
});

Template.registerHelper('options', () => {
	return {
		states: [
			'AL',
			'AK',
			'AS',
			'AZ',
			'AR',
			'CA',
			'CO',
			'CT',
			'DE',
			'DC',
			'FL',
			'GA',
			'HI',
			'ID',
			'IL',
			'IN',
			'IA',
			'KS',
			'KY',
			'LA',
			'ME',
			'MD',
			'MA',
			'MI',
			'MN',
			'MS',
			'MO',
			'MT',
			'NE',
			'NV',
			'NH',
			'NJ',
			'NM',
			'NY',
			'NC',
			'ND',
			'OH',
			'OK',
			'OR',
			'PA',
			'PR',
			'RI',
			'SC',
			'SD',
			'TN',
			'TX',
			'UT',
			'VT',
			'VA',
			'WA',
			'WV',
			'WI',
			'WY',
		]
	};
});

Template.registerHelper('selectedIf', (value, compare) => {
	var selected = '';
	if( ! compare.hash ) {
		selected = (value == compare) ? 'selected' : '';
	} else {
		selected = value ? 'selected' : '';
	}
	return selected;
});

Template.registerHelper('selectedIfInArray', (needle, haystack) => {
	var selected = '';
	if(haystack && ! haystack.hash ) {
		selected = (haystack.indexOf(needle) > -1) ? 'selected' : '';
	} else if(! haystack) {
		selected = needle ? 'selected' : '';
	}
	return selected;
});

Template.registerHelper('checkedIfInArray', (needle, haystack) => {
	var checked = '';
	if(haystack && ! haystack.hash) {
		checked = (haystack.indexOf(needle) > -1) ? 'checked' : '';
	} else if(! haystack) {
		checked = needle ? 'checked' : '';
	}
	return checked;
});

Template.registerHelper('checkedIf', (value, compare) => {
	var checked = '';
	if( ! compare.hash ) {
		checked = (value == compare) ? 'checked' : '';
	} else {
		checked = value ? 'checked' : '';
	}
	return checked;
});

Template.registerHelper('categories', () => Category.find());

Template.registerHelper('categoryById', (id) => Category.findOne({_id: id}).name);

Template.registerHelper('moment', (date, format) => moment(date).format(format));

Template.registerHelper('DateFrequency', () => DateFrequency);

Template.registerHelper('sponsors', () => Sponsor.find());

months = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December',
];

import '../imports/api/main.js';
import '../imports/startup/client/subscriptions.js';
import '../imports/startup/client/routes.js';
import '../imports/startup/client/config.js';
import '../imports/ui/components/logo.html';
