Events = new Mongo.Collection('events');

if(Meteor.isServer) {
	Events._ensureIndex({'location.lngLat': '2dsphere'});
}

Duration = Astro.Class.create({
	name: 'Duration',
	fields: {
		allDay: Boolean,
		start: Date,
		end: Date
	}
});

DateRange = Astro.Class.create({
	name: 'Date range',
	fields: {
		start: {
			type: Date,
			default: new Date()
		},
		end: {
			type: Date,
			default: new Date()
		},
	}
});

DateFrequency = Astro.Enum.create({
	name: 'Date frequency',
	identifiers: ['DAILY', 'WEEKLY', 'MONTLHY', 'YEARLY']
});

Recurrence = Astro.Class.create({
	name: 'Recurrence',
	fields: {
		duration: {
			type: Duration,
			optional: true
		},
		dateRange: {
			type: DateRange,
			optional: true
		},
		frequency: {
			type: DateFrequency,
			optional: true
		},
		interval: {
			type: Number,
			optional: true
		},
		count: {
			type: Number,
			optional: true
		}
	},
	methods: {
		frequencyUnit: function () {
			var unit;
			switch(this.frequency) {
				case 0: unit = 'day'; break;
				case 1: unit = 'week'; break;
				case 2: unit = 'month'; break;
				case 3: unit = 'year'; break;
			}
			return unit;
		}
	}
});

LngLat = Astro.Class.create({
	name: 'Longitude/Latitude',
	fields: {
		lng: Number,
		lat: Number
	},
	methods: {
		asArray: function () {
			return [this.lng, this.lat];
		}
	}
});

Location = Astro.Class.create({
	name: 'Location',
	fields: {
		name: String,
		city: {
			type: String,
			optional: true
		},
		country: {
			type: String,
			optional: true
		},
		lngLat: {
			type: LngLat,
			optional: true
		},
		longitude: {
			type: Number,
			optional: true,
			resolve (doc) {
				return doc.lngLat ? doc.lngLat.lng : 0;
			}
		},
		latitude: {
			type: Number,
			optional: true,
			resolve (doc) {
				return doc.lngLat ? doc.lngLat.lat : 0;
			}
		},
		state: {
			type: String,
			optional: true
		},
		street: {
			type: String,
			optional: true
		},
		zip: {
			type: String,
			optional: true
		}
	},
	methods: {
		getLatlng: function () {return [this.latitude, this.longitude];},
		getLngLat: function () {return [this.longitude, this.latitude];},
		getString: function () {
			return this.street + ' ' + this.city + ', ' + this.state + ' ' + this.zip;
		}
	}
});

EventRemote = Astro.Class.create({
	name: 'Event Remote Source',
	fields: {
		id: String,
		name: String,
		params: {
			type: Object,
			optional: true
		}
	},
	methods: {
		link: function () {
			var resolver = EventRemote.linkResolvers[this.name];
			if(typeof resolver === 'function') {
				return resolver(this);
			} else {
				console.log(resolver, 'is not a function of', this.name);
			}
		},
		logo: function () {
			return EventRemote.logos[this.name];
		}
	}
});

EventRemote.linkResolvers = {};

EventRemote.addLinkResolver = (name, resolver) => {
	EventRemote.linkResolvers[name] = resolver;
};

EventRemote.logos = {};

EventRemote.addLogo = (name, logo) => {
	EventRemote.logos[name] = logo;
};

Event = Astro.Class.create({
	name: 'event',
	collection: Events,
	fields: {
		title: String,
		description: String,
		categoryIds: [String],
		duration: {
			type: Duration,
			optional: true
		},
		recurrence: {
			type: Recurrence,
			optional: true
		},
		location: Location,
		cost: {
			type: String,
			optional: true,
			default () {
				return 'free';
			}
		},
		ageLimit: {
			type: String,
			optional: true
		},
		publisher: {
			type: String,
			optional: true
		},
		image: {
			type: Object,
			optional: true
		},
		attendees: {
			type: [String],
			optional: true,
			default () {
				return [];
			}
		},
		remote: {
			type: EventRemote,
			optional: true
		},
		addlRemotes: {
			type: [EventRemote],
			optional: true,
			default: () => []
		}
	},
	methods: {
		categories: function () {
			return Category.find({_id: {$in: this.categoryIds}});
		},
		getImage: function (options = {}) {
			var imageURL = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
			if(!options.store) options.store = 'images';
			if(this.image) {
				let image = Images.findOne({_id: this.image._id});
				if(image) imageURL = image.url({store: options.store});
			}
			return imageURL;
		},
		categoryGradient: function () {
			var gradient = '';
			if(this.categoryIds.length > 1) {
				this.categoryIds.forEach((index, id) => {
					var category = Category.findOne({_id: id});
					gradient += `#${category.color}`;
					if(index + 1 !== this.categoryIds.length) gradient += ', ';
				});
			} else if(this.categoryIds.length > 0) {
				var category = Category.findOne({_id: this.categoryIds[0]});
				gradient = `#${category.color}, white`;
			} else {
				gradient = '#00b8d4, #303F9F';
			}
			return gradient;
		},
		currentUserIsAttending: function () {
			var user = User.findOne({_id: Meteor.userId()});
			return user && user.eventIds && user.eventIds.indexOf(this._id) > -1;
		},
		sponsors: function () {
			return Sponsor.find({
				$or: [
					{sponsorships: {$elemMatch: {
						collectionName: 'Category',
						id: {$in: this.categoryIds}
					}}}
				]
			});
		},
		sponsorLinks: function () {
			var sponsors = this.sponsors().fetch();
			var string = '';
			if(sponsors.length) {
				string = 'Listing sponsored by ';
				sponsors.forEach((sponsor, index) => {
					if(index > 0) {
						if(index < sponsors.length - 1) {
							if(sponsors.length > 2) {
								string += ', ';
							} else {
								string += ' ';
							}
						} else {
							string += ', and ';
						}
					}
					string += `<a target="_blank" href="${sponsor.url}">${sponsor.name}</a>`;
				});
			}
			return string;
		},
		recurrences: function () {
			var recurrences = [];
			if(this.recurrence instanceof Recurrence && (this.recurrence.dateRange || this.recurrence.count)) {
				console.log('recurrence', this.recurrence);
				var startDate = moment(this.recurrence.dateRange.start);
				var endDate = moment(this.recurrence.dateRange.end);
				var currentDate = startDate;
				var recurrence;
				var duration = this.recurrence.duration;
				recurrence = this.copy();
				recurrence.recurrence = false;
				recurrence.duration = {}
				recurrence.duration.start = moment(this.recurrence.dateRange.start);
				recurrence.duration.start.hour(moment(this.recurrence.duration.start).hour());
				recurrence.duration.start = recurrence.duration.start.toDate();
				recurrence.duration.end = moment(this.recurrence.dateRange.start);
				recurrence.duration.end.hour(moment(this.recurrence.duration.end).hour());
				recurrence.duration.end = recurrence.duration.end.toDate();
				recurrences.push(recurrence);
				while (currentDate.toDate() < endDate.toDate() && (!this.recurrence.count || recurrences.length < this.recurrence.count)) {
					switch(this.recurrence.frequency) {
						case DateFrequency.DAILY:
						currentDate.add(this.recurrence.interval, 'days');
						break;
						case DateFrequency.WEEKLY:
						currentDate.add(this.recurrence.interval, 'weeks');
						break;
						case DateFrequency.MONTHLY:
						currentDate.add(this.recurrence.interval, 'months');
						break;
						case DateFrequency.YEARLY:
						currentDate.add(this.recurrence.interval, 'years');
						break;
						default:
						break;
					}
					recurrence = this.copy();
					// recurrence = this;
					recurrence.recurrence = false;
					recurrence.duration = {};
					// console.log(recurrence.duration);
					recurrence.duration.start = moment(this.recurrence.duration.start);
					recurrence.duration.start.year(currentDate.year()).dayOfYear(currentDate.dayOfYear());
					recurrence.duration.start = recurrence.duration.start.toDate();
					// console.log(recurrence.duration.start);
					recurrence.duration.end = moment(this.recurrence.duration.end);
					recurrence.duration.end.year(currentDate.year()).dayOfYear(currentDate.dayOfYear());
					recurrence.duration.end = recurrence.duration.end.toDate();
					// console.log(recurrence);
					recurrences.push(recurrence);
					recurrence = false;
				}
			}
			return recurrences;
		},
		start: function () {
			return moment(this.duration.start).format('hh:mm MM/DD/YYYY');
		},
		end: function () {
			return moment(this.duration.end).format('hh:mm MM/DD/YYYY');
		},
		timespan: function () {
			var timespan = '';
			if(this.duration.allDay) {
				timespan = moment(this.duration.start).format('MMM Do');
			} else {
				var startFormat = 'h:mma [on] MMM Do';
				var endFormat = startFormat;
				var start = moment(this.duration.start);
				var end = moment(this.duration.end);
				if(start.dayOfYear() === end.dayOfYear()) {
					startFormat = startFormat.replace(' [on] MMM Do', '');
					if(start.format('a') === end.format('a')) {
						startFormat = startFormat.replace('a', '');
					}
				}
				if(start.minute() === 0) {
					startFormat = startFormat.replace(':mm', '');
					if(start.hour() === 12) {
						if(start.format('a') === 'am') {
							startFormat = startFormat.replace('h', '[Midnight]');
						} else {
							startFormat = startFormat.replace('h', '[Noon]');
						}
					}
				}
				if(end.minute() === 0) {
					endFormat = endFormat.replace(':mm', '');
					if(end.hour() === 12) {
						if(end.format('a') === 'am') {
							endFormat = endFormat.replace('h', '[Midnight]');
						} else {
							endFormat = endFormat.replace('h', '[Noon]');
						}
					}
				}
				if(start.hour() === 0 && start.minute() === 0 && end.hour() === 0 && end.minute() === 0 && this.duration.allDay === false) {
					startFormat = startFormat.replace(/h|\:|\[on\]|a/gi, '').trim();
					endFormat = endFormat.replace(/h|\:|\[on\]|a/gi, '').trim();
					this.duration.allDay = true;
					this.save();
				}
				if(start.unix() === end.unix()) {
					timespan = start.format(endFormat);
				} else {
					timespan = `${start.format(startFormat)} - ${end.format(endFormat)}`;
				}
				if(end.unix() === 0) {
					timespan = `${start.format(startFormat)}`;
				}
			}
			return timespan;
		},
		gCalLink: function () {
			// http://stackoverflow.com/questions/10488831/link-to-add-to-google-calendar
			return `http://www.google.com/calendar/event?action=TEMPLATE&text=${encodeURIComponent(this.title)}&dates=${this.duration.start.toISOString().replace(/-|:|\.\d\d\d/g, '')}/${this.duration.end.toISOString().replace(/-|:|\.\d\d\d/g, '')}&details=${encodeURIComponent(this.description)}&location=${encodeURIComponent(this.location.getString())}&trp=false&sprop=&sprop=name:`;
		},
		mergeWith: function (duplicateEvent) {
			// This function takes another event "duplicateEvent," takes useful data from it and then deletes "duplicateEvent" and saves `this`
			if(duplicateEvent.remote) {
				if(this.remote) {
					this.addlRemotes.push(duplicateEvent.remote);
				} else {
					this.remote = duplicateEvent.remote;
				}
				duplicateEvent.addlRemotes.forEach(remote => {
					this.addlRemotes.push(remote);
				});
			}
			if(!this.ageLimit && duplicateEvent.ageLimit) {
				this.ageLimit = duplicateEvent.ageLimit;
			}
			duplicateEvent.attendees.forEach(attendee => {
				if(!this.attendees.indexOf(attendee)) this.attendees.push(attendee);
			});
			duplicateEvent.categoryIds.forEach(categoryId => {
				if(this.categoryIds.indexOf(categoryId) < 0) this.categoryIds.push(categoryId);
			});
			if(this.cost === 'free' && duplicateEvent.cost !== 'free') {
				this.cost = duplicateEvent.cost;
			}
			if(this.description !== duplicateEvent.description) {
				this.description += '<hr>' + duplicateEvent.description;
			}
			if((this.duration.start === this.duration.end) && (duplicateEvent.duration.start !== duplicateEvent.duration.end)) {
				this.duration = duplicateEvent.duration;
			}
			if(!this.image && duplicateEvent.image) {
				this.image = duplicateEvent.image;
			}
			if(duplicateEvent.location) {
				if(!this.location.city && duplicateEvent.location.city) {
					this.location.city = duplicateEvent.location.city;
				}
				if(!this.location.country && duplicateEvent.location.country) {
					this.location.country = duplicateEvent.location.country;
				}
				if(!this.location.lngLat && duplicateEvent.location.lngLat) {
					this.location.lngLat = duplicateEvent.location.lngLat;
				}
				if(!this.location.name && duplicateEvent.location.name) {
					this.location.name = duplicateEvent.location.name;
				}
				if(!this.location.state && duplicateEvent.location.state) {
					this.location.state = duplicateEvent.location.state;
				}
				if(!this.location.street && duplicateEvent.location.street) {
					this.location.street = duplicateEvent.location.street;
				}
				if(!this.location.zip && duplicateEvent.location.zip) {
					this.location.zip = duplicateEvent.location.zip;
				}
			}
			if(!this.recurrence && duplicateEvent.recurrence) {
				this.recurrence = duplicateEvent.recurrence;
			}
			this.save((error, id) => {
				if(error) {
					console.error(error);
				} else {
					duplicateEvent.remove();
				}
			});
		}
		// shortDescription: () => this.description.substr(0, 500)
	},
	events: {
		beforeInsert (event) {
			var isDuplicate = false;
			var eventsWithSameName = Event.find({title: event.target.title}).fetch();
			eventsWithSameName.forEach(dup => {
				if(event.target.duration.start > dup.duration.start - 1800 || event.target.duration.start < dup.duration.start + 1800) {
					isDuplicate = dup;
				}
			});
			if(event.target.location.lngLat) {
				var eventsAtSameSpaceTime = Event.find({
					$and: [
						{
							'location.lngLat': {
								$near: {
									$geometry: {
										type: 'Point',
										coordinates: event.target.location.lngLat.asArray()
									},
									$maxDistance: 400,
									$minDistance: 0
								}
							}
						},
						// {
						// 	$where: 'event.target.duration.start > this.duration.start - 1800 || event.target.duration.start < this.duration.start + 1800'
						// }
					]
				}).fetch();
				if(eventsAtSameSpaceTime > 0) {
					console.log('eventsAtSameSpaceTime', eventsAtSameSpaceTime);
				}
			}
			if(isDuplicate) {
				var duplicate = Event.findOne({_id: isDuplicate._id});
				duplicate.addlRemotes.forEach(remote => {
					if(!(remote.name === event.target.remote.name || remote.id === event.target.remote.id)) {
						duplicate.addlRemotes.push(event.target.remote);
						duplicate.save();
					}
				});
				event.preventDefault();
			}
		},
		afterSave (event) {
			// event.preventDefault();
			if(event.target.location && (!event.target.location.latitude || !event.target.location.longitude)) {
				HTTP.get(
					'https://maps.googleapis.com/maps/api/geocode/json',
					{
						params: {
							address: event.target.location.getString(),
							key: 'AIzaSyCQHoFTTUs-jxU7xj2Wdex_WjjLtxhbUms'
						}
					},
					(error, response) => {
						response = JSON.parse(response.content);
						if(!error && response.results[0]) {
							event = Event.findOne({_id: event.target._id});
							event.location.lngLat = new LngLat();
							event.location.lngLat.lat = Number(response.results[0].geometry.location.lat);
							event.location.lngLat.lng = Number(response.results[0].geometry.location.lng);
							event.save();
						} else {
							console.log('error geocoding', error);
						}
					}
				);
			}
		}
	},
	secured: false
});
