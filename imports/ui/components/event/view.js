/*
*     ___            __  ___    __        __
*    |__  |  | |\ | /  `  |  | /  \ |\ | /__`
*    |    \__/ | \| \__,  |  | \__/ | \| .__/
*
*/

function calculateCalendarSpan (calendar) {
	var title = '';
	switch(calendar.getView().name) {
		case 'month':
			if(Number(calendar.getDate().format('YYYY')) !== Number(moment().format('YYYY'))) {
				title = calendar.getDate().format('MMMM ′YY');
			} else {
				title = calendar.getDate().format('MMMM');
			}
			break;
		case 'agendaDay':
			title = calendar.getDate().format('MMMM Do');
			break;
		case 'agendaWeek':
			const view = calendar.getView();
			const start = view.intervalStart;
			const end = view.intervalEnd;
			if(start.format('MMMM') === end.format('MMMM')) {
				title = `${start.format('MMMM Do')} - ${end.format('Do')}`;
			} else {
				title = `${start.format('MMMM Do')} - ${end.format('MMMM Do')}`;
			}
			break;
	}
	Session.set('calendarSpan', title);
}

/*
*     __           __   __   ___      ___  ___  __
*    /  \ |\ |    /  ` |__) |__   /\   |  |__  |  \
*    \__/ | \|    \__, |  \ |___ /~~\  |  |___ |__/
*
*/

Template.eventCard.onCreated(function () {
	this.event = () => Event.findOne({_id: this.data._id});
});

Template.eventView.onCreated(function () {
	Session.set('calendarView', false);
	this.event = () => Event.findOne({_id: FlowRouter.getParam('eventId')});
});

Template.eventList.onCreated(function () {
	var params = {};
	this.category = new ReactiveVar($('[name=categoryFilter]').val() || this.data.category || 'all');
	if(this.data.orderBy) {
		params.sort = {};
		switch(this.data.orderBy) {
			case 'date': params.sort['duration.start'] = 1; break;
			default: break;
		}
	}
	this.events = () => {
		var currentMonth = moment(Session.get('calendarSpan'), 'MMMM');
		var startDate = currentMonth.clone().date(1).subtract(5, 'days').toDate();
		var endDate = currentMonth.clone().date(currentMonth.daysInMonth()).add(5, 'days').toDate();
		console.log('enddate', endDate);
		var query = {
			$and: [
				{$or: [{
					'duration.start': {
						$gte: startDate
					}
				}, {
					'recurrence': {$exists: true}
				}]},
				{
					'duration.start': {
						$lte: endDate
					}
				}
			]
		};
		if(this.category.get() !== 'all') {
			query.categoryIds = {
				$in: [this.category.get()]
			};
		}
		return Event.find(query, params);
	};
	this.renderHeaders = callback => {
		this.$('.list__header').remove();
		var events = this.events().fetch();
		var dates = {};
		events.forEach(event => {
			var start = moment(event.duration.start).startOf('day');
			if(!dates[start.unix()]) {
				dates[start.unix()] = event._id;
			}
		});
		for (var date in dates) {
			if (dates.hasOwnProperty(date)) {
				var html = `<span class="list__header">${moment(date, 'X').format('dddd MMMM Do')}</span>`;
				var element = this.find('#' + dates[date]);
				$(html).insertBefore(element);
			}
		}
		callback();
	};
	this.heightOfHeaders = () => {
		var height = 37.75;// height of list header
		$('header, select[name=categoryFilter]').each((index, element) => {
			height += element.getBoundingClientRect().height;
		});
		return height;
	};
	this.jumpTo = (monthName, offset = this.heightOfHeaders()) => {
		var monthStart = moment().month(monthName).date(1).toDate();
		var scrollTo;
		var earliest;
		this.events().fetch().forEach(event => {
			if(event.duration && event.duration.start > monthStart) {
				if(event.duration.start < earliest || !earliest) {
					scrollTo = event._id;
					earliest = event.duration.start;
				}
			}
		});
		window.scrollTo(0, this.$('#' + scrollTo)[0].offsetTop - offset);
		// this.$('#' + scrollTo).prev('.list__header')[0].scrollIntoView({behavior: 'smooth'});
	};
});

Template.eventCalendar.onCreated(function () {
	this.category = new ReactiveVar($('[name=categoryFilter]').val() || this.data.category || 'all');
	this.events = () => {
		var query = {};
		if(this.category.get() !== 'all') {
			query.categoryIds = {
				$in: [this.category.get()]
			};
		}
		var events = [];
		events = events.concat(Event.find(query).fetch().map(event => {
			if(event.recurrence && event.recurrence.duration) {
				events.push(event);
				event.recurrences().forEach(recurrence => {
					recurrence.start = recurrence.duration.start;
					recurrence.end = recurrence.duration.end;
					recurrence.url = FlowRouter.path('eventView', {eventId: event._id});
					events.push(recurrence);
				});
			} else if (event.duration) {
				event.start = event.duration.start;
				event.end = event.duration.end;
				event.url = FlowRouter.path('eventView', {eventId: event._id});
				return event;
			}
			return false;
		}));
		// Return only the events that exist
		return _.filter(events, event => event);
	};
});

/*
*     __           __   ___       __   ___  __   ___  __
*    /  \ |\ |    |__) |__  |\ | |  \ |__  |__) |__  |  \
*    \__/ | \|    |  \ |___ | \| |__/ |___ |  \ |___ |__/
*
*/
Template.eventCalendar.onRendered(function () {
	var self = this;
	var calendarSpan = Session.get('calendarSpan') ? Session.get('calendarSpan').split(' ')[0] : moment().format('MMMM');
	this.$('#events-calendar').fullCalendar({
		header: {
			left: 'month',
			center: 'agendaWeek',
			right: 'agendaDay'
		},
		eventLimit: true,
		firstDay: 1,
		views: {
			month: {},
			basic: {},
			agenda: {},
			week: {},
			day: {}
		},
		events (start, end, timezone, callback) {
			var data = self.events();
			if(data) callback(data);
		},
		viewRender (view) {
			calculateCalendarSpan(this.calendar);
		},
		dayClick (date, event, view) {
			console.log(this, date, event, view);
			self.$('#events-calendar').fullCalendar('changeView', 'agendaDay');
			self.$('#events-calendar').fullCalendar('gotoDate', date);
		},
		eventLimitClick: 'day'
	});
	// this.$('#events-calendar').fullCalendar.on
	this.$('#events-calendar').fullCalendar('gotoDate', moment(calendarSpan, 'MMMM'));
	this.autorun(() => {
		Event.find().fetch();
		this.$('#events-calendar').fullCalendar('refetchEvents');
	});
	this.$('#events-calendar').fullCalendar('option', 'height', (function () {
		var height = window.innerHeight;
		$('header, #homeView, select[name=categoryFilter], .fc-toolbar').each((index, element) => {
			height -= $(element).height();
		});
		return height;
	})());
	if(!Session.get('calendarSpan')) Session.set('calendarSpan', this.$('#events-calendar').fullCalendar('getDate').format('MMMM'));
});

Template.eventList.onRendered(function () {
	this.renderHeaders(() => {
		var calendarSpan = Session.get('calendarSpan') ? Session.get('calendarSpan').split(' ')[0] : moment().format('MMMM');
		this.jumpTo(calendarSpan, 0);
		Session.set('calendarSpan', calendarSpan);
	});
	var headers = this.$('.list__header');
	var minHeight = $('header').height() + $('select[name=categoryFilter]').height() + 2;
	this.currentHeader = {};
	window.onscroll = () => {
		var lastDistance = 0;
		var shouldContinueLooking = true;
		var i = 0;
		var header;
		var lastHeader = this.currentHeader;
		while (shouldContinueLooking && headers[i]) {
			var distance = headers[i].getBoundingClientRect().top;
			if(distance > lastDistance && distance > minHeight && i > 0) {
				shouldContinueLooking = false;
				header = headers[i - 1];
			} else {
				lastDistance = distance;
				i++;
			}
		}
		if(header && header !== lastHeader) {
			this.currentHeader = header;
			headers.removeClass('fixed');
			$(header).addClass('fixed');
			var month = moment(header.textContent, 'dddd MMMM Do').format('MMMM');
			Session.set('calendarSpan', month);
			// TODO If current month changes, scroll back to top *of month* (not window)
		}
	};
});

Template.eventView.onRendered(function () {
	window.scrollTo(0, 0);
	var self = this;
	var initPlaces = function () {
		// console.log(self.event());
		var event = Event.findOne({_id: FlowRouter.getParam('eventId')});
		if( event && event.location.lngLat ) {
			var mapElement = document.getElementById('map');
			if(mapElement) {
				self.map = new google.maps.Map(mapElement, {
					center: event.location.lngLat,
					zoom: 15,
				});
				new google.maps.Marker({
					position: event.location.lngLat,
					map: self.map
				});
			}
		}
	};
	if( ! window.google ) {
		$.getScript('https://maps.googleapis.com/maps/api/js?key=' + Meteor.settings.public.googleAPIKey + '&libraries=places', initPlaces);
	} else {
		initPlaces();
	}
});

/*
*     __           __   ___  __  ___  __   __       ___  __
*    /  \ |\ |    |  \ |__  /__`  |  |__) /  \ \ / |__  |  \
*    \__/ | \|    |__/ |___ .__/  |  |  \ \__/  |  |___ |__/
*
*/
Template.eventList.onDestroyed(function () {
	window.onscroll = false;
});
/*
*     ___       ___      ___  __
*    |__  \  / |__  |\ |  |  /__`
*    |___  \/  |___ | \|  |  .__/
*
*/
Template.eventCalendar.events({
	'click .fc-event' (event, template) {
		var history = Session.get('history');
		history.push(window.location.href);
		Session.set('history', history);
	}
});

Template.eventList.events({
	'click .event__link' (event, template) {
		var history = Session.get('history');
		history.push(window.location.href);
		Session.set('history', history);
	}
});

Template.eventView.events({
	'click .event__save' (DOMevent, template) {
		var event = template.event();
		var user = User.findOne({_id: Meteor.userId()});
		if(event.currentUserIsAttending()) {
			user.removeEvent(event._id);
		} else {
			user.saveEvent(event._id);
		}
	},
	'click .event__location' (event, template) {
		template.$('#map').addClass('active');
		window.scrollTo(0, template.$('.event__content')[0].offsetTop);
		Session.set('mapOpen', true);
	}
});

/*
*          ___       __   ___  __   __
*    |__| |__  |    |__) |__  |__) /__`
*    |  | |___ |___ |    |___ |  \ .__/
*
*/
Template.eventCard.helpers({
	event () {
		return Template.instance().event();
	}
});

Template.eventList.helpers({
	events () {
		return Template.instance().events();
	}
});

Template.eventView.helpers({
	event () {
		return Template.instance().event();
	}
});
