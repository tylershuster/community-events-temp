/*
*     __           __   __   ___      ___  ___  __
*    /  \ |\ |    /  ` |__) |__   /\   |  |__  |  \
*    \__/ | \|    \__, |  \ |___ /~~\  |  |___ |__/
*
*/
Template.sponsorCategory.onCreated(function () {
	this.category = () => Category.findOne({_id: FlowRouter.getParam('categoryId')});
});

Template.sponsorNew.onCreated(function () {
	this.logo = new ReactiveVar();
});

/*
*     __           __   ___       __   ___  __   ___  __
*    /  \ |\ |    |__) |__  |\ | |  \ |__  |__) |__  |  \
*    \__/ | \|    |  \ |___ | \| |__/ |___ |  \ |___ |__/
*
*/
Template.sponsorNew.onRendered(function () {
	if(FlowRouter.getParam('categoryId')) {
		this.$('[name=sponsorCategory]').val(FlowRouter.getParam('categoryId'));
	}
});
/*
*     __           __   ___  __  ___  __   __       ___  __
*    /  \ |\ |    |  \ |__  /__`  |  |__) /  \ \ / |__  |  \
*    \__/ | \|    |__/ |___ .__/  |  |  \ \__/  |  |___ |__/
*
*/

/*
*     ___       ___      ___  __
*    |__  \  / |__  |\ |  |  /__`
*    |___  \/  |___ | \|  |  .__/
*
*/
Template.sponsorCategory.events({
	'click .remove' (event, template) {
		let sponsor = Sponsor.findOne({_id: template.$(event.target).parents('.category__sponsor').attr('id')});
		let index = sponsor.sponsorships.findIndex(sponsorship => sponsorship.collectionName === 'Category' && sponsorship.id === FlowRouter.getParam('categoryId'));
		sponsor.sponsorships.splice(index, 1);
		console.log(sponsor);
		sponsor.save();
	},
	'change h2 input' (event, template) {
		let sponsor = Sponsor.findOne({_id: template.$(event.target).parents('.category__sponsor').attr('id')});
		sponsor.name = event.target.value;
		sponsor.save();
	},
	'change h3 input' (event, template) {
		let sponsor = Sponsor.findOne({_id: template.$(event.target).parents('.category__sponsor').attr('id')});
		sponsor.url = event.target.value;
		sponsor.save();
	},
	'change input[name=logo]' (event, template) {
		let sponsor = Sponsor.findOne({_id: template.$(event.target).parents('.category__sponsor').attr('id')});
		FS.Utility.eachFile(event, file => {
			Images.insert(file, (err, fileObj) => {
				sponsor.logo = fileObj;
				sponsor.save();
			});
		});
	},
	'submit #sponsorAdd' (event, template) {
		event.preventDefault();
		// console.log(event.ta)
		let sponsor = Sponsor.findOne({_id: template.find('[name=sponsorId]').value});
		if(sponsor) {
			if(!sponsor.sponsorships) sponsor.sponsorships = [];
			sponsor.sponsorships.push(new Sponsorship({
				collectionName: 'Category',
				id: template.category()._id
			}));
			sponsor.save((error, id) => {
				if(!error) {
					event.target.value = '';
				}
			});
		} else {
			FlowRouter.go('/sponsor/new/' + template.category()._id);
		}
	}
});

Template.sponsorNew.events({
	'submit #sponsorNew' (event, template) {
		event.preventDefault();
		console.log(template);
		let sponsor = new Sponsor({
			name: template.find('[name=name]').value,
			url: template.find('[name=url]').value,
			logo: template.logo.get()
		});
		let sponsorship = new Sponsorship({
			collectionName: 'Category',
			id: template.find('[name=sponsorCategory]').value
		});
		sponsor.sponsorships = [sponsorship];
		console.log(sponsor);
		sponsor.save((error, id) => {
			console.log(error, id);
			if(!error) {
				if(FlowRouter.getParam('categoryId')) {
					FlowRouter.go('/sponsor/category/' + FlowRouter.getParam('categoryId'));
				} else {
					FlowRouter.go('/sponsors');
				}
			}
		});
	},
	'change [name=logo]' (event, template) {
		FS.Utility.eachFile(event, file => {
			Images.insert(file, (err, fileObj) => {
				console.log(err, fileObj);
				template.logo.set(fileObj);
			});
		});
	}
});

/*
*          ___       __   ___  __   __
*    |__| |__  |    |__) |__  |__) /__`
*    |  | |___ |___ |    |___ |  \ .__/
*
*/
Template.sponsorCategory.helpers({
	category () {
		return Template.instance().category();
	},
	categorySponsors () {
		const sponsors = Sponsor.find({
			sponsorships: {$elemMatch: {
				collectionName: 'Category',
				id: Template.instance().category()._id
			}}
		});
		console.log(sponsors.fetch());
		return sponsors;
	}
});



/*
*     ___            __  ___    __        __
*    |__  |  | |\ | /  `  |  | /  \ |\ | /__`
*    |    \__/ | \| \__,  |  | \__/ | \| .__/
*
*/
