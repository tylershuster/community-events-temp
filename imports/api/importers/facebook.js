Meteor.startup(() => {
	if(Meteor.isServer) {
		facebookCategories = ApplicationDatum.findOne({name: 'Facebook Categories'});
		if(!facebookCategories) {
			facebookCategories = new ApplicationDatum();
			facebookCategories.name = 'Facebook Categories';
			facebookCategories.pairs = {};
			facebookCategories.pairs.timestamp = new Date();
			facebookCategories.pairs.ART_EVENT = Categories.findOne({name: 'Arts'})._id;
			facebookCategories.pairs.BOOK_EVENT = Categories.findOne({name: 'Family & Education'})._id;
			facebookCategories.pairs.MOVIE_EVENT = Categories.findOne({name: 'Hobbies'})._id;
			facebookCategories.pairs.FUNDRAISER = Categories.findOne({name: 'Charity & Causes'})._id;
			facebookCategories.pairs.VOLUNTEERING = Categories.findOne({name: 'Charity & Causes'})._id;
			facebookCategories.pairs.FAMILY_EVENT = Categories.findOne({name: 'Family & Education'})._id;
			facebookCategories.pairs.FESTIVAL_EVENT = Categories.findOne({name: 'Community'})._id;
			facebookCategories.pairs.NEIGHBORHOOD = Categories.findOne({name: 'Community'})._id;
			facebookCategories.pairs.RELIGIOUS_EVENT = Categories.findOne({name: 'Spirituality'})._id;
			facebookCategories.pairs.SHOPPING = Categories.findOne({name: 'Hobbies'})._id;
			facebookCategories.pairs.COMEDY_EVENT = Categories.findOne({name: 'Arts'})._id;
			facebookCategories.pairs.MUSIC_EVENT = Categories.findOne({name: 'Music'})._id;
			facebookCategories.pairs.DANCE_EVENT = Categories.findOne({name: 'Arts'})._id;
			facebookCategories.pairs.NIGHTLIFE = Categories.findOne({name: 'Home & Lifestyle'})._id;
			facebookCategories.pairs.THEATER_EVENT = Categories.findOne({name: 'Arts'})._id;
			facebookCategories.pairs.DINING_EVENT = Categories.findOne({name: 'Food & Drink'})._id;
			facebookCategories.pairs.FOOD_TASTING = Categories.findOne({name: 'Food & Drink'})._id;
			facebookCategories.pairs.CONFERENCE_EVENT = Categories.findOne({name: 'Community'})._id;
			facebookCategories.pairs.MEETUP = Categories.findOne({name: 'Hobbies'})._id;
			facebookCategories.pairs.CLASS_EVENT = Categories.findOne({name: 'Family & Education'})._id;
			facebookCategories.pairs.LECTURE = Categories.findOne({name: 'Family & Education'})._id;
			facebookCategories.pairs.WORKSHOP = Categories.findOne({name: 'Family & Education'})._id;
			facebookCategories.pairs.FITNESS = Categories.findOne({name: 'Sports & Fitness'})._id;
			facebookCategories.pairs.SPORTS_EVENT = Categories.findOne({name: 'Sports & Fitness'})._id;
			facebookCategories.pairs.OTHER = Categories.findOne({name: 'Other'})._id;
			facebookCategories.save();
		}
	}
	let facebookCrawler = options => {
		if(Meteor.isServer) {
			let parseFacebookEvent = facebookEvent => {
				let eventExists = Event.findOne({
					$and: [
						{'remote.id': facebookEvent.id},
						{'remote.name': 'facebook'},
					]
				});
				let event;
				if( eventExists ) {
					event = eventExists;
					event._isNew = false;
				} else {
					event = new Event();
					event._isNew = true;
					event.remote = new EventRemote();
					event.remote.name = 'facebook';
					event.remote.id = facebookEvent.eventId;
				}
				event.title = facebookEvent.eventName;
				event.description = facebookEvent.eventDescription;
				event.duration = event.duration || {};
				event.duration.allDay = event.duration.allDay || false;
				event.duration.start = event.duration.start || moment(facebookEvent.eventStarttime).toDate();
				event.duration.end = event.duration.end || moment(facebookEvent.eventEndtime).toDate();
				event.location = event.location || {};
				event.location.name = event.location.name || facebookEvent.venueName;
				event.location.city = event.location.city || facebookEvent.venueLocation.city;
				event.location.country = event.location.country || facebookEvent.venueLocation.country;
				event.location.state = event.location.state || facebookEvent.venueLocation.state;
				event.location.street = event.location.street || facebookEvent.venueLocation.street;
				event.location.zip = event.location.zip || facebookEvent.venueLocation.zip;
				event.location.lngLat = event.location.lngLat || {};
				event.location.lngLat.lat = event.location.lngLat.lat || facebookEvent.venueLocation.latitude;
				event.location.lngLat.lng = event.location.lngLat.lng || facebookEvent.venueLocation.longitude;
				event.categoryIds = event.categoryIds || [facebookCategories.pairs[facebookEvent.category]];
				event.cost = event.cost || 'Free';
				event.ageLimit = event.ageLimit || '';
				event.owner = event.owner || '';
				event.image = event.image || {};
				event.attendees = event.attendees || [];
				event.save((saveError, eventId) => {
					if(facebookEvent.eventProfilePicture) {
						let imageURL = facebookEvent.eventCoverPicture;
						Images.insert(imageURL, (imageError, fileObj) => {
							// event = Event.findOne({_id: event._id});
							event.image = fileObj;
							event.save();
						});
					}
				});
			};
			let getFacebookEvents = options => {
				// Configure URLs & parameters
				let queryURL = 'http://tylers-27-imac.local:3333/events' +
				'?lat=' + options.latitude +
				'&lng=' + options.longitude +
				'&distance=' + Math.round(options.within * 1609.344) +
				'&access_token=' + '1591312174525890|8f76d916d5caa70b3bd5ffb35fb0de99';
				// Get list of events within range
				HTTP.get(queryURL, {}, (errorGet, response) => {
					if(!errorGet) {
						let events = JSON.parse(response.content).events;
						// Loop through each return event
						events.forEach(facebookEvent => {
							parseFacebookEvent(facebookEvent);
						});
					}
				});
			};
			return getFacebookEvents(options);
		}
	};
	FacebookEvents = new EventSource({
		name: 'facebook',
		crawler: facebookCrawler,
		linkResolver: remote => 'https://www.facebook.com/events/' + remote.id,
		logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowNTgwMTE3NDA3MjA2ODExODA4M0NDMTM4MEMyQTVFQiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCOEE0QzYzNUE2MTYxMUUyOEJFQUJDRTMzOERDQjM5MCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCOEE0QzYzNEE2MTYxMUUyOEJFQUJDRTMzOERDQjM5MCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUM3QUJGQTkzODIwNjgxMThDMTQ5OEFGOTgxQUJBQ0UiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDU4MDExNzQwNzIwNjgxMTgwODNDQzEzODBDMkE1RUIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz79NQmWAAACYUlEQVR42uzcv2sTYRwG8CdnLjG/sFKbEu1QFKWlViOFQGtRWhx0UIQiSp2sm5NLwbo5xX/AsTqJBenSuqqLUIQiQURHlaZia2OS5tpcTGz9nmaR3pueoKbJPQ88We44jg/3vve+BM5z8vIkJN3SpHRYGoG7U5A+ld6WvvNWceake8Cg+oBclA5JBzT5uUsc21gmSa06rBj7DFtAYTooE9ZoUDsEIhCBCFTPeBv55mNtEZxKdOJ4VxSdHS1o2xtEYLf+81i5sgGzVMZ6sYLCWgnZVRPLmXWpgcnHr5obqOdwO8YuxZHoPQCPx/4c3atJ/YiE/GjfF/rtWNMC6fou3BhNYORsFzSVjFuHWFCGTnL8DPp6Ypykt9yk5sGdm0P/HadhgK5eOIb+eAdf83aJtoZxbSTOdZAqo+d74ZPJmUA28fu8OHf6EFfSqvQdjSEU8BFIlRPdsbrfw45eBx052Or43K95E1NP3mAutYDPXwwUzXLzA+2POvuDJZMr4vrEDFaya+4aYpGQs/nnwXTqn+DseKBgQHd03stU2p2TtNMN6dKK4e6txnbZ2NwkENdBBCIQgQjEEKgh9mIvpsbqdr17D+fxaPY1nyBVPqSzHGK18n4hRyBVzFIFS5kCgZTDazGHP922uQroYzrP13zN+WcxS6DaQ4xP0Dav+ByBVPlW/o5Py6uNsZIevHL/r66QnV6PezECEYhABCIQgRgCEYhABCIQgQhEIIZABCIQgQhEIAIRyNVABhmUMSygZ3RQ5rkFNCHN02JLLJNbFtBb6YB0Fr8+kef2FKoW/ZbNDwEGAIpDioOQ+EZDAAAAAElFTkSuQmCC'
	});
});
