Deps.autorun(function () {
	Meteor.subscribe('me');
	Meteor.subscribe('events');
	Meteor.subscribe('categories');
	Meteor.subscribe('images');
	Meteor.subscribe('sponsors');
});
