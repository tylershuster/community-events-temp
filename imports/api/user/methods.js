Meteor.methods({
	'/user/save': function (user) {
		user._isNew = false;
		user.save();
		return user;
	}
});
