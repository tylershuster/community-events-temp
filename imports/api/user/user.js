User = Astro.Class.create({
	name: 'User',
	collection: Meteor.users,
	fields: {
		name: {
			type: String,
			optional: true
		},
		avatar: {
			type: Object,
			optional: true
		},
		services:{
			type: Object,
			optional: true
		},
		eventIds: {
			type: [String],
			optional: true,
			default: () => []
		},
		subscriptions: {
			type: Object,
			optional: true,
			fields: {
				calendars: [String],
				categories: [String]
			},
			default: () => {
				return {
					calendars: [],
					categories: []
				};
			}
		},
		admin: {
			type: Boolean,
			default: false
		}
	},
	methods: {
		saveEvent: function (eventId) {
			this.eventIds = this.eventIds || [];
			this.eventIds.push(eventId);
			Meteor.call('/user/save', this);
			// this.save();
		},
		removeEvent: function (eventId) {
			var attendingIndex = this.eventIds.indexOf(eventId);
			if(attendingIndex > -1) {
				this.eventIds.splice(attendingIndex, 1);
				Meteor.call('/user/save', this);
				// this.save();
			}
		}
	}
});
