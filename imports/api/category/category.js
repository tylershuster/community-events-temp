Categories = new Mongo.Collection('categories');

Category = Astro.Class.create({
	name: 'Category',
	collection: Categories,
	fields: {
		name: String,
		parent: {
			type: String,
			optional: true
		},
		color: String
	}
});
