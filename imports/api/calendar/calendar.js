Calendars = new Mongo.Collection('calendars');

Calendar = Astro.Class.create({
	name: 'Calendar',
	collection: Calendars,
	fields: {
		title: String,
		description: String,
		image: {
			type: Object,
			optional: true
		},
		owner: String
	},
	methods: {
		getImage (options = {}) {
			var imageURL = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
			if(!options.store) options.store = 'images';
			if(this.image) {
				let image = Images.findOne({_id: this.image._id});
				if(image) imageURL = image.url({store: options.store});
			}
			return imageURL;
		}
	}
});
