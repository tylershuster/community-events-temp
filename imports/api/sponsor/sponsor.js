Sponsors = new Mongo.Collection('sponsors');

Sponsorship = Astro.Class.create({
	name: 'Sponsorship',
	fields: {
		collectionName: String,
		id: String
	},
	methods: {
		collection () {
			return eval(this.collectionName);
		}
	}
});

Sponsor = Astro.Class.create({
	name: 'Sponsor',
	collection: Sponsors,
	fields: {
		logo: Object,
		name: String,
		url: {
			type: String,
			optional: true
		},
		sponsorships: {
			type: [Sponsorship],
			optional: true
		}
	},
	methods: {
		logoSrc (options = {}) {
			var logoURL = 'data:logo/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
			if(!options.store) options.store = 'images';
			if(this.logo) {
				let logo = Images.findOne({_id: this.logo._id});
				if(logo) logoURL = logo.url({store: options.store});
			}
			return logoURL;
		}
	},
	secured: false
});
