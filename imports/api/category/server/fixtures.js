if(Meteor.isServer) {
	let category = Category.findOne({name: "Hobbies"});
	if( ! category ) {
		Categories.insert({
			name: "Hobbies",
			color: "ff9800"
		});
	}
	category = Category.findOne({name: "Health"});
	if( ! category ) {
		Categories.insert({
			name: "Health",
			color: "f44336"
		});
	}
	category = Category.findOne({name: "Business"});
	if( ! category ) {
		Categories.insert({
			name: "Business",
			color: "3f51b5"
		});
	}
	category = Category.findOne({name: "Arts"});
	if( ! category ) {
		Categories.insert({
			name: "Arts",
			color: "9c27b0"
		});
	}
	category = Category.findOne({name: "Family & Education"});
	if( ! category ) {
		Categories.insert({
			name: "Family & Education",
			color: "ff5722"
		});
	}
	category = Category.findOne({name: "Charity & Causes"});
	if( ! category ) {
		Categories.insert({
			name: "Charity & Causes",
			color: "00bcd4"
		});
	}
	category = Category.findOne({name: "Other"});
	if( ! category ) {
		Categories.insert({
			name: "Other",
			color: "9e9e9e"
		});
	}
	category = Category.findOne({name: "Spirituality"});
	if( ! category ) {
		Categories.insert({
			name: "Spirituality",
			color: "673ab7"
		});
	}
	category = Category.findOne({name: "Food & Drink"});
	if( ! category ) {
		Categories.insert({
			name: "Food & Drink",
			color: "795548"
		});
	}
	category = Category.findOne({name: "Community"});
	if( ! category ) {
		Categories.insert({
			name: "Community",
			color: "8bc34a"
		});
	}
	category = Category.findOne({name: "Sports & Fitness"});
	if( ! category ) {
		Categories.insert({
			name: "Sports & Fitness",
			color: "ff5722"
		});
	}
	category = Category.findOne({name: "Travel & Outdoor"});
	if( ! category ) {
		Categories.insert({
			name: "Travel & Outdoor",
			color: "4caf50"
		});
	}
	category = Category.findOne({name: "Home & Lifestyle"});
	if( ! category ) {
		Categories.insert({
			name: "Home & Lifestyle",
			color: "009688"
		});
	}
	category = Category.findOne({name: "Music"});
	if( ! category ) {
		Categories.insert({
			name: "Music",
			color: "e91e63"
		});
	}
}
