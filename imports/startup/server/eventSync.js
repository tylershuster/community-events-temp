eventSyncImporters = [];

eventSync = function () {
	var latitude = 40.5864;
	var longitude = -122.3917;
	var within = 20;
	eventSyncImporters.forEach(importer => {
		importer({latitude, longitude, within});
	});
	lastEventSync = ApplicationDatum.findOne({name: 'Last Event Sync'});
	lastEventSync.pairs.timestamp = new Date();
	lastEventSync.save();
};

lastEventSync = ApplicationDatum.findOne({name: 'Last Event Sync'});
if(!lastEventSync) {
	lastEventSync = new ApplicationDatum();
	lastEventSync.name = 'Last Event Sync';
	lastEventSync.pairs = {timestamp: new Date()};
	lastEventSync.save();
	eventSync();
}

Events.before.find((userId, selector, options) => {
	if(Date.now() - lastEventSync.pairs.timestamp.getTime() > 300000) {
		eventSync();
	}
});
