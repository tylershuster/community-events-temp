EventSource = function (options = {}) {
	if(options.name) {
		if(Meteor.isServer && options.crawler) {
			eventSyncImporters.push(options.crawler);
		}
		if(options.linkResolver) {
			EventRemote.addLinkResolver(options.name, options.linkResolver);
		}
		if(options.logo) {
			EventRemote.addLogo(options.name, options.logo);
		}
	}
};
import './importers/eventbrite.js';
import './importers/facebook.js';
import './importers/eventful.js';
import './importers/q97.js';
import './importers/visitredding.js';
import './importers/enjoy.js';
import './importers/meetup.js';
import './importers/kixe.js';
import './importers/nspr.js';
import './importers/actionnewsnow.js';
