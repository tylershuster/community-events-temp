Meteor.startup(() => {
	let actionNewsCrawler = options => {
		if(Meteor.isServer) {
			var pagesCrawled = [];
			var crawler = new Crawler({
				maxConnections: 10,
			});
			let crawlActionNewsNowPage = (error, result, $) => {
				if(!error) {
					var eventExists = Event.findOne({'remote.id': result.uri});
					var event = eventExists || new Event();
					event.remote = event.remote || new EventRemote();
					event.remote.name = 'actionnewsnow';
					event.remote.id = result.uri;
					event.title = $('h3 span').text() || event.title;
					event.description = $('th:contains(Description:)').next('td').text();
					event.duration = event.duration || {};
					var eventDate = $('th:contains(Event Date)');
					if(eventDate.length > 0) {
						eventDate = eventDate.next('td');
						eventStart = $('th:contains(Start Time)').next('td').text();
						eventEnd = $('th:contains(End Time)').next('td').text();
						event.duration.start = moment(eventDate.text() + ' ' + eventStart, 'M/D/YYYY h:mm A').toDate();
						event.duration.end = moment(eventDate.text() + ' ' + eventEnd, 'M/D/YYYY h:mm A').toDate();
					}
					event.location = event.location || {};
					event.location.name = $('th:contains(Location)').next('td').text();
					event.ageLimit = event.ageLimit || '';
					event.publisher = event.publisher || '';
					event.categoryIds = [];
				} else {
					console.log('error', error);
				}
			};
			let crawlActionNewsNowArchive = (error, result, $) => {
				$('.date-events a').each((index, event) => {
					var url = 'http://www.actionnewsnow.com' + $(event).attr('href');
					if(pagesCrawled.indexOf(url) < 0) {
						pagesCrawled.push(url);
						crawler.queue([{
							uri: url,
							callback: Meteor.bindEnvironment((error, result, $) => crawlActionNewsNowPage(error, result, $))
						}]);
					}
				});
				// $('a.next').each((index, link) => {
				// 	var linkValue = Number($(link).text());
				// 	var href = 'http://actionnewsnow.org' + $(link).attr('href');
				// 	if(linkValue && linkValue > currentPage && pagesCrawled.indexOf(href) < 0) {
				// 		pagesCrawled.push(href);
				// 		crawler.queue([{
				// 			uri: href,
				// 			callback: Meteor.bindEnvironment((error, result, $) => crawlActionNewsNowArchive(error, result, $))
				// 		}]);
				// 	}
				// });
			};
			let getActionNewsNowEvents = options => {
				var url = 'http://www.actionnewsnow.com/community-calendar/';
				pagesCrawled.push(url);
				crawler.queue([
					{
						uri: url,
						callback: Meteor.bindEnvironment((error, result, $) => crawlActionNewsNowArchive(error, result, $))
					}
				]);
			};
			return getActionNewsNowEvents(options);
		}
	};
	ActionNewsNow = new EventSource({
		name: 'actionnewsnow',
		crawler: actionNewsCrawler,
		linkResolver: remote => remote.id,
		logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGgAAABmCAYAAAAnFtTRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowODc2MUI2OUM0QjNFMzExOEMyRkQwMjI5NUZBNjg3MyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpBNDJCMzM4OUNCQjUxMUUzQURFNEMwMTE0OUVBRUFFNCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpBNDJCMzM4OENCQjUxMUUzQURFNEMwMTE0OUVBRUFFNCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjdDNzVBQTJCMzBDN0UzMTFCMUYwQTZFMDNCQjUxNjI1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjA4NzYxQjY5QzRCM0UzMTE4QzJGRDAyMjk1RkE2ODczIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+RzFyngAAGQNJREFUeNrsXQd8U9X+/zVN23Rv2gKl7A0yOqCgLFFAVBAHy/FUnuBTcTBkKQ4ciP4VfY/1RH0PfQIt0ycoS0Zry1JWgUqhe1I606RN2vx/v9smuefemzQNSRvfJ7/P59c0N+eee+7ve37znNy4QAtJp9MNxpcXkUchhyN7IruAkyTFhaxCLkQ+ivy5i4vLby3pwMVCUGT4sgx5PnKwU+63RaXInyGvQrAabhsgBOdpfFmL7O2UrU1JifwSgrTZKoAQGAW+HEEe5pSlXSkFeQwCpbYYIAQnEl9+Rw5yyq9V6BbyIAQpp1mAmsC57DRpbWLy+ghBchGA44UvuciBTnm1CZUhd0SQakwBdApfop1yalM6jQDFiABCcJ7Dl/VO+TgEzUWQNhgAaspzyAYqnLJxCKKIzpvyJFnTgVVOcByKFE2YGDSInFOAUy4OReWoQYFyBGekExyHpADCxgX/fIdvZjjl4ZD0HwIoC//p5JSFQ1I2AaRyBgiOG80RQA3gXM9xVNIRQDqnHByXWh2g8vJyuHjhIhQU5EOtWg3+AQHQpUsX6NOnL7jKXR1CKA0NDZCeng5/IFfgeN3c3CE0NBT69usH4RHhjgvQE7Nmw4Xz55ljO/fsgc5dOjd77onjx2HThg2QnJQMUpf08/ODhx6eBnOffx5CQkIMx7/dsgXWfLja6hv09fWFY8lJMHjAQOb4gsWLYNbs2cyxW7duwZebNsH2rdu4/6WoT58+8NQzT8OUqVPB1ZWdUG8uXwF7du82vPfw8IDE3bugQ4cOJsd334QJkJ+Xb3i/afNmiI4xlkPllt5oxrVrKNwk0fGEbdu4mzVFKpUKli9dCrt37jLbf2VlJXy9+SvYmbgDPvhoNYy/5x7ueF1dHVRVVd32TBT2Qf3y6dCBg7B44UJOw83R5cuXYfGChfCfb7+Dz//xd4iIiGDulX8d+n/F0mWw+ZuvTfZXXVXNnFNfr2U+l1l6g9twVknRjh07sNN6yc9ICHOefqZZcPhUUVEBf5s7D47+8kurmZH//vADzHvuuWbB4dPvv/0GD099CPLz8822O3b0KPywd6/VY7NIg7RaLc7sRMnPiouK4PixYzB6zBjRZ6vefgdSfv2VOebp6cmZh7jhwyAgIBCysjIhcft2OH/uPOMDFr22AA4cOQxRnaLg7vHjRX1fz8iA69evG96T/xozZqyonZeXl9l7S7+aDgtffY27Jp/uGDQIHpwyBaI6R3GaceniJUhMSODuV09FhYXwwrx5sB213pz/fGflWzDyzjvxfgPsA9DBAwdM2uRGM7ddBNClixc5/8EnutnNX3/Dvepp5J0jYeasWfD5Z5/B2k8/MxwvLS2Fbd9vhWf/OgfG3j1OdM21n37KtPfy9IL1mza2WABvvfmmyNwtWbYUnn72WXBxMWYfEyZOhDnP/RXmv/AiNyH1RBNr69bvuXswRXQvH6x6jzPdLSWLTBwBwKcZM2eKACy7VcYc++fGTcx7mmEb0AHzwTFEKiiIl15+mQOLT/t+/NGupu3ihQuQmpIiurdn5sxhwOEHMv9Yv07k9Ddt2AjNxVoJaCV+TU62PUCFBYWcHTUIGiOX+a++Av3692dM4O5du3iOrh6OHD7M9PPgg1Oge48eZq/1HJoLMmd67tCxg10BOnzokGgSvfTKy2bP8USTOfdvzzPHcrKzMST/Q9RWaNIoWKqtrbUtQIQ83z6TLaUw+IEHHxBFc/yZWV1dzXw+YdKkZgczPD6eM1N6XvvFF3YFSDijo6NjuHynOZowYaLomFATiR6bMZ3L8fSUlZmFpnyt7QAitSWA+DTloanc6+T772fMwJUrVzhguBmVI9o9BP369XW4LD0rK5t5T4moJRQYFMiE10TZWVmidpQHrfrgfdYcbtwAV69ctQ1ANMNyecL28vYyRFRh4eEQExsr0LaExmpBWZmor2Be8ukoJBxncLDl2wADAgNFgYAUxcbFwSOPPWY0/9p6WLp4sShqtAogYXBAqk1hsp7uF5i5PeiHKCKSuYpDTlO5UluSsBJQX99g+cmCoEAuNx0Qv750CVMdOXfuHPz7m3/dHkCUMO7ft0/SvOlpIvoV/sCoGvDzTz9BUGCQZLDhaBQUFCQYY4HF5woT1OAQ098p8Pf3hxUr32SOfbzmo2aTXLMAUfYvzA+oFte9cxcDRw8azEVwwmChR6+eov7OnjnT7GAunL/A1cz4TMmgvahnr17M+zNnTltW9sIkmSYwn3r07Gn2nPsmT4YxY42JdI2yBlauWGE9QNu3bbPqppNOJHFmkHxUS/uj8g7VpfSs93X2oviRI0RVBZokzVGiIHDi+oof0XxS/O47XJhuDPMPN6tFMlMJ3OW0NKtumiK/HQmJojD8ZGoq7Nqx07TDLi9Hu/wNc0yqfGRLmoQmWuiHVr7xhtlchaLVb75ii58xsTEWLUO0b98eXluwQCSvFpd6tguCg65du8LC1xeb7GT71q3cbODnTtt3JMJ3W74FpVJpdJaLFnGaQWURfu0qLy+PK6EII6HpM+27l4W0cwaOZcu/jA773O+/wzNP/QU+RB8hrBj8cuQILFm0WATgy6++avE1H3/yCUzqd1qkqZIAqdVqLhrj00OPPGwo/0sRxft8gCg0p+WJN99+iyt68isOVPv6Yu1aiI6JAf8Af24thLRL6O/unTAB4obZ/6tJCxYthOPHjnJJpJ6owDvmzrtgaPRQiIrqzMmEaos3btwQnT/7iSdaNE7S2Pc+/BCmTL7foshWBND+H/cx6xOUjFJV1xyNGDmSi4j4BVXSojWffALFxcWiBTfSlJ/27zfjvHuKEjx7kY+PD1fAnT1jBhTwojjKU06dPMWxKaJJu2zF8hZfkxb9qAi8YV3zW+FlzQUHlIwKs2apWTFRUMohoKncM3fePPj7+nXQrl07iwZPmvMfHIM1pXlriQq4O/fuMWslhBaDFinpvtzc3Ky65ovz50Nkp04t0yBVTQ107NgROkybZgwP759s0QXJX9C6CZ+uZ1yHgXcM5IQ+avRoLkig3IoWu/i1uo6RkRA/Ih6mz5jJtbdoFvbtCw/xxslPoKWI35YLiwWFW0ok123cwCWRieiDk5OTOLOnd+IESm+c+ePuHgcPP/qo5IQjsy0coylSKBSwes0azn8z4xDUAttsVw8BRD7J18fXYTaLCIn8Yg0GOVQZob0NUksQ9ibntisHJ5eUErUTIEcGqHNirhMgByaZUwSOTXKnCG6PXDFu6ObrBuGeriDH/ys1DZBRpYWyuoa2Bejz2CAYHORuLNfUaGHWiVLQNkhbzB5+cvgq3rgmUq1tgAkHi7kbSxgVatUYfsxTwd5cFayLY5cNZp24CVnVWlH7OT184MluPob3f1Rp4C9J0gttm+ODoaefMcf56lo1fHnNmBpE+cjhb718YWJ7T/BxE0d3BNK2LCX8O0MJqnpd6wMUqpBBBy9jeEz/kwDWXZXeBeqGISq/fZWm8aZo1vGPt4SC3GVwpUIDwR4yULgahRQb7C4J0N0RCuZaETg5fN1kOBZ2tnvhoO4KUwCvS7iE19HTI1FesGpwALjJTIfd3XzlsKS/PzzZ1QeeTi6Fq5WatvdBL/X2hSjv1rWaGtTY1Jts8TI62EPUzhOlPYSn8dzNo3xjgt1FbYfiMT44pAFnShtrheMR5NVDA82Cw6f2OCG23BnCTaI290E0i9/FmfU4mpjboSylFmq0zZuFfFVjsTGpuBZGhRm/gxYTIhb6EBS6XEKow0I94HAh+xyjWAHAKSW13EQgWoRawafcmnr4JK0SziKALk3WgMwoAWmoDiA4c3v6wqoLFW0fJIxs5wEPRnrB7pwaq/t4/Ww5JxRL6UQx27YL+geasaW1Dcy4pIgAEpIQYH3/nbHf7r6syJ5B85XOM1+ZaFppwmwaHsyZVD0RYNYAZJcwe8VAf/B3a70InvzQrdoGs2ZuWIg0QP383Tg/ZPCVqGWDg6QBaqcQ+8rrEr6OaFumknlvrZ+1mRTreNEbzd4lA/xbDSCdhBZF83wLATAw0Pi+ghcUCP3QHYFu4M4zhUXqeoOGKLXi0PkNnIy+EpORzOYdewsMPPS/hVZ9z9RmAG1Mr2ZAeqyzl6QvsIS80Jf54U2bY4q0+JRUojZppggAvvv55x/VJs1cjEDTjhcZgb9aqRVp6uNdveHMfeHwJYbls7p4c9EbEUXWlBPx2Zpg22Y+iBz751eq4LW+foZj7w0OhEmHig0O1lKim22OKHKbfuymST/UP8Cdi9woAhsh8D8JWTUwvbO3wezwAYoTTKokXr+U462+VAEfDGE3LZJZHBuu4JioEIOXX9GHHkEt+rlADbW3kQfZ1FFsQC26VmW0yeRQ5/b0aRUzl4/RVCbPH1CYPKjJlwznAUATiQTID0L0fojOGRok0KBiVjO3ZtbA2+crGGshJEq+p3bygrWYzB+7Nwzu6+jpGACRpiz9jd1O+wLmRhRVtQYdl/BDgZjM9vZ344XMjflMCi930vuhXn5uTFUgDYOP0lqx36Gqwrifi7jKQl6N+X0FFFh8gUBREu8QtbhTN+vge4xgyIQQkcOl3Oi9FoSY69OruFKJOSpRiwVH5oh8gtH3eHAazfdWemCEYTyZuXyBsE8UmQ71Kf95FzWJmCZAPJ5Pmkrm1NNVHA4sxvyJTN61Zu6rVYqlH1yoxLjf05A90+BJ5S2loyiYluRBeiK7T5ZHHxBQRSCnRiuYQLUGAdPs5/uhXKXWrHkzF+YTb0aN8kBwRoV5wPw+ftCXp7mE2QOYH1JS2+Z5EIWx75xnNeapbvb3RRQpXSivY2pq/ImR1wQKv0LA90N8X0WO/VQpuxVs+6hQ+HFcOwOPDhM/QYfO+zlfDVOOlIi0RZjkthlARFRJ4EdWrq20nH9cYJb45kaolUI/FOBuFAeBI4y+qGTTB4HUc7yJ6oTeH58uZa9X29DyaA7HpQNrWEguHLNtlmPAYC7EbGxn6rh1nFyiNhua89ummjGjJ9C8CfsWCpzynn7+cslx+GGwcVc7VsPS0Qy29H7sGl5lc7lRJSzo17Kqwj+Hh2CiZ1mxdOLBIuYYVZ3VOCkUEior1KBc9E98P8Rqohjof19XwsNR3owJTRzdjlv3OYbtizFwIWD6YQ42G4OV9rx+KYei9as2j+JEFQbM2sk58he/mq0kyF3Akgdw+Wh0kqaFNGWUwD9Q7pNbo5UEbVoUG8BQaH2lQrx+c6GsDtZeroSX+hiTcQoKHu/qw7E5oiQ+W6ltuYmzN0DaptyoNXemJBXXSpo3U2ZPyryZGu+nCBClDGoLqwM0YT7GyO2LK5VW3Yu8wUrJXSrXAP8uinCGmurr9M3GmcevKCvxBqk9lWJSrQipuVyoVvqalG+MC2c16MdclWRbAkN4/R9yVGBOLhsxT9uLQdBs1Jp7Oyi4PQlCfb9RrYVDBSrYgmYxs1pr9WRziUrIcW67uk3yRpMc5unK+T2VVseZyEqNjTaNuDg3lt421aAvvCEAxFZZhRzACZAjE2qQUwgODZDuT6xBVIil0r4n+oCKugbMQ+qhQefUoGYp0lsOywexi1rbblTDoXzTidqjXXxgXHvjugm1pXOERKupM7v5wv2dvKA3JoR8W0+hbwpGZQmZ1bA/t4YB66kevjCcl9lTtXz1+TLJsbwfHQxBvG1SW3EchyXG3hHvc4XgPpefuQUl6nobAmQH1EmI4zuwyd/gYA8Yvy/fZHRDwuafQxm+cGxTMYt/Y0gQ178UURQ1OsKT47TyOng19Sb80ZRwUsmJ3/8IjLY+uVAGwnQmVOEKjwmSTtoCdkQCoGGhCqbPUgz7S9X1Nn3GtUzX0AA2Z50YhBC88YUDAsycw0qK3vM/f6WfP6yJCzEJjpD6IuAJY8O5TSB0flJhjahaQcsBwnFIFUBjQj0kxxwbyi6PH8e8p8HGspRxgrADS9GM7r4wBDVJsr3wHN5nD3Tyhhf6ib+zeq1SA99dq4R1aeWwC81apWDDug+C+eVdYRCGkyOrSgM5goRxaIh4LHGh4iWE9l5yrl4nbBsjaHu8UGVzOcpldsiDZCa6JNV/PzYEJu/LFW2yd5FoS2Pzc5fBymj2SVmUDC5JLYa9WdUiE7dsSAjM7GGslQV6uMLKocHw/PFCToumd/fjaYYCvr7CPkh2RLj0/gHyX4nXqxhTGOXD1hd/LagBW8sTTRyZEtuzKerh7w5zevuLz5E0cTqYiQLlr9MQvZpcBHtuVIn6UKF/W47A7clkN/DfG+kNXVGYJwpYMxeNAPHP74Ca0tHE3vK4dp5M21iB9qSjzytUamwuRxmQv7A1A2tqhHi9OCAIOvm4Cs4Rgqrjjk+OYh12apEKfsqqNHv9906XiLZ6Te7sjTNcyVyF/CJt59WfN1xQAVfx9ocPo89414gRrPWcwL7tIUsZ0IPlbM6scJQ4sxOuVTAl+ndj27HnSOAT5O4CfQJZp70ro6LZ6xcr6yApn916OyzME8pUGrhUqhZokYfhvPgwT6YKvyPDOOYO3m7QkdZ3mtpGh7Km8EReNdhDljIX/GMPFuUWJwvhFi8/GNneG6Z28TWeIzBx9L67r3gN6bcipUXX/71EJTCtHtxxTpAMQJ6Gc4aFG0PmczdV8EtOlQhkauePEWBv3v450taTBUq7yNE+GiQKs3VQjrN3VSr7wLxlsWEQQPvQuHPEUVyIQhxSF1XXWTSGgmp2w0cgmjO64SQhQKQ1eLybnxuEeRn9Twpq4KmCasYYDAvzatQeNG/8oOZMUQ2o6rR20qB6TKxszCDxaEk6vvtqKSTzBBSkkMPr0WGN50gA5CZRhtJqtFaNgQTqhhPnbH4ls0+iK2pWME6S4WGsyTqZVwXVqjpIKzVqYlyEF9e3yLzlVII95Egsa8AbsTXrJEyc/rNlR7OhjiegR3oH4Sz2EkV+9L5UKV52pojOkjH4CyI/8oO1mnqc6fVwuoDVoiEYoQ1vbwxG6mlHTn4V109KntHMdfBx53Ki2Aj2Z86PZVeCPeRIbCcfJLHrp+mz7DIVfHGaNXWr7ooE4TcEyQdll4s3bgwIVlg0hv4h7CzPwr70n9GMZyoF4d4QxwPoQkkNqNFkUdvUXLbtmEhfGBBq9FXlai1cLq4Ge/lyuYvWDk/jFT4HDfHiX2fTqTx4sEcgdAtqFGK3QAWEewsCAjRHubdqoKCqDiJ8jSWVSd0C4Of0UrOX95TLYGxnf4HJqjSMITkTk9Phxof1TesdDAEKo/9JzTG2Tc2q4HYYuTY9p+fZQWHMVymTUHt0mnq7/cYc+iCy6bbmegkfZPxcW6eBZT9fY5eN3V0FGqfj2h7MYMGY1CMYBoQozF5/XnQ4+Aj6O5B+0/D5lcIKbuYbzKaCTU5PZpcZ2qrUtXCx0GgSI/3ZsD8pswzsI8NGlsk02gpksCmLtFInavNbVhlsO2f6MchciInt/pXKloVo8q5/oBf0Q62Tuvas/qEwL64j09c59CdnbtwytAE0XymZ0ksNpC1ncWz8Pk9mmf5doV8zboLN5WfkCrlMo6FnQQ60pVq6aMW7WPA6omMfH0iHcd2DIdhb4pt4HEAayC3RwLrjN+DFUV0NH4X5ekDCk4Ph4NUSOIXCrEJtCPdTwD19QqF/hB8b9SG47+y7DC4aDWOGklEzJ/QWP0DjEoKpVqqZ/WinbpTCnGGRorY3SmugsLTannvXsuRojtJsDlBDvWSYLaQqZT18+NNVWP3QAHF7nc5wzsajGdA3zAfG9TY+RI/8wIQ+7Tg2RQ3Yx9JdFyEtt1zkI1KuldAqlOicM9m3RGM9m1nK+CEjyDcl78uGlCbX1Wnot2em27JXnUYrLqvVST9pY++ZbJgyMALiu7MVax3euP4c6m3+t6fhtXt7w1MjuoIlz9Urr6mDZTvOw+HLRZKf5xRrIAeDkMggdmHxJAInHKsS31/KrYCBkeySR3J6scn7shEdldfX1e23da+16FjzyoyVYyWaoHrBU3359EbCWdg8ZwS48qKj8mo1cw7N0/d3n4NdpzJhzuiecHf/CFC4ifdUF1aoYOfpbPjq2DUOJHO0/3weTLqDffTyyfQivK5Y6Acv5UOwjzs/j4bkKwXYVmtPgPZzEuk2f+sv+DLqz7SZws1VBj0i/CEiwJP7nxLQGyVVkH2z+n9lv8jRjM8eG90YX9ZqPv2zAURzPC2jGNLgf5Y+5XwtF2HV1dKvs563dbDgJKuJsNjNrDT3nr0+nvIup2wcgkZc2TI3mQGIqOe0z9bgy2tO+bQpfZyeON/wewpMjaNepV6CL4ORxzrl1CZ0BHkJkw8KW/Qa9y5VGQ9Qkdcpr1Yl+pGI8VcPLa8wCxAH0rDlBBL9NvQ4p9xahegHXaddTXlX9LQPkzl5z/4Lyfy9g7wYAFycMrQLURX4Q+QV6Rc/ksx4mxV8r74L4vCFfnE22ilPmxL9YN4LV9PWpJprZJFmIEjUbgLyK01mz/lAdOuoocmc/R/yfgSn2W2oLTZdCFb7JrCo8kA/6N0ZOcgpe0miX7zKRL6IfLQJlPyWdPD/AgwAZ1WzXyjwQhsAAAAASUVORK5CYII='
	});
});
