Meteor.startup(() => {
	if(Meteor.isServer) {
		var pagesCrawled = [];
		var crawler = new Crawler({
			maxConnections: 10,
		});
		nsprCategories = ApplicationDatum.findOne({name: 'NSPR Categories'});
		if(!nsprCategories) {
			crawler.queue([{
				uri: 'http://mynspr.org/community-calendar',
				callback: Meteor.bindEnvironment((error, result, $) => crawlNSPRCategories(error, result, $))
			}]);
		}
		crawlNSPRCategories = (error, result, $) => {
			nsprCategories = new ApplicationDatum();
			nsprCategories.name = 'NSPR Categories';
			nsprCategories.pairs = {};
			nsprCategories.pairs.timestamp = new Date();
			$('#search-filters .category-list a').each((index, category) => {
				var catID = $(category).attr('href').replace('/community-calendar?category_id=', '');
				switch (catID) {
					case '2327': nsprCategories.pairs[2327] = Categories.findOne({name: 'Arts'})._id; break;
					case '2329': nsprCategories.pairs[2329] = Categories.findOne({name: 'Family & Education'})._id; break;
					case '2336': nsprCategories.pairs[2336] = Categories.findOne({name: 'Community'})._id; break;
					case '2332': nsprCategories.pairs[2332] = Categories.findOne({name: 'Arts'})._id; break;
					case '2338': nsprCategories.pairs[2338] = Categories.findOne({name: 'Food & Drink'})._id; break;
					case '2333': nsprCategories.pairs[2333] = Categories.findOne({name: 'Charity & Causes'})._id; break;
					case '2330': nsprCategories.pairs[2330] = Categories.findOne({name: 'Family & Education'})._id; break;
					case '2331': nsprCategories.pairs[2331] = Categories.findOne({name: 'Family & Education'})._id; break;
					case '2334': nsprCategories.pairs[2334] = Categories.findOne({name: 'Arts'})._id; break;
					case '2328': nsprCategories.pairs[2328] = Categories.findOne({name: 'Music'})._id; break;
					case '2337': nsprCategories.pairs[2337] = Categories.findOne({name: 'Community'})._id; break;
					case '2335': nsprCategories.pairs[2335] = Categories.findOne({name: 'Arts'})._id; break;
				}
			});
			nsprCategories.save();
		};
	}
	let nsprCrawler = options => {
		if(Meteor.isServer) {
			let crawlNSPRPage = (error, result, $) => {
				if(!error) {
					var eventExists = Event.findOne({'remote.id': result.uri});
					var event = eventExists || new Event();
					event.remote = event.remote || new EventRemote();
					event.remote.name = 'nspr';
					event.remote.id = result.uri;
					event.title = $('h1.title').text() || event.title;
					event.description = $('.description p').text();
					event.duration = event.duration || {};
					if($('.date').text().indexOf(' to ') > 0) {
						// recurring, do something else
					} else {
						var timespan = $('.time').text().split(' - ');
						event.duration.allDay = false;
						event.duration.start = moment($('.date').text() + ' ' + timespan[0], 'MMMM D, YYYY h:mm a').toDate();
						event.duration.end = moment($('.date').text() + ' ' + timespan[1], 'MMMM D, YYYY h:mm a').toDate();
					}
					event.location = event.location || {};
					event.location.name = $('.event-venue li:nth-of-type(1)').text();
					event.location.street = $('.event-venue li:nth-of-type(2)').text();
					event.location.country = 'US';
					var city = $('.event-venue li:nth-of-type(3)').text();
					event.location.city = city.split(',')[0];
					if(city.split(',')[1]) {
						event.location.state = city.split(',')[1].trim().split(' ')[0];
						event.location.zip = city.split(',')[1].trim().split(' ')[1];
					}
					event.cost = $('.price').text();
					event.categoryIds = event.categoryIds || [];
					$('.category a').each((index, categoryLink) => {
						var id = $(categoryLink).attr('href').replace('/community-calendar?category_id=', '');
						var ccCategoryId = nsprCategories.pairs[id];
						if(event.categoryIds.indexOf(ccCategoryId) < 0) event.categoryIds.push(ccCategoryId);
					});
					event.ageLimit = event.ageLimit || '';
					event.publisher = event.publisher || '';
					event.attendees = event.attendees || [];
					if($('.event-image img').length > 0) {
						var imageURL = $('.event-image img').attr('src');
						Images.insert(imageURL, (imageError, fileObj) => {
							event.image = fileObj;
							event.save((error, eventId) => {
								// console.log('saved event', error, eventId);
							});
						});
					} else {
						event.save((error, eventId) => {
							// console.log(error, eventId);
						});
					}
					// console.log(event);
				} else {
					console.log('error', error);
				}
			};
			let crawlNSPRArchive = (error, result, $) => {
				if(!error) {
					$('.event-stub h2 a').each((index, event) => {
						var url = 'http://mynspr.org' + $(event).attr('href');
						if(pagesCrawled.indexOf(url) < 0) {
							pagesCrawled.push(url);
							crawler.queue([{
								uri: url,
								callback: Meteor.bindEnvironment((error, result, $) => crawlNSPRPage(error, result, $))
							}]);
						}
					});
					var currentPage = Number(result.uri.replace('http://mynspr.org/community-calendar?page=', '')) || 1;
					$('.pager-item a').each((index, link) => {
						var linkValue = Number($(link).text());
						var href = 'http://mynspr.org' + $(link).attr('href');
						if(linkValue && linkValue > currentPage && pagesCrawled.indexOf(href) < 0) {
							pagesCrawled.push(href);
							crawler.queue([{
								uri: href,
								callback: Meteor.bindEnvironment((error, result, $) => crawlNSPRArchive(error, result, $))
							}]);
						}
					});
				} else {
					console.log(error);
				}
			};
			let getNSPREvents = (options) => {
				var url = 'http://mynspr.org/community-calendar';
				pagesCrawled.push(url);
				crawler.queue([
					{
						uri: url,
						callback: Meteor.bindEnvironment((error, result, $) => crawlNSPRArchive(error, result, $))
					}
				]);
			};
			return getNSPREvents(options);
		}
	};
	nspr = new EventSource({
		name: 'nspr',
		crawler: nsprCrawler,
		linkResolver: remote => remote.id,
		logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHMAAABaCAYAAACL4GkYAAAgAElEQVR4nOy9d7Bn2VXf+1k7nPALN/TtOD3TkzUzGmmkURgJRkIghEBIgBFgsGWMeebh8sOIZCjKfn742djP2IApbEQwKoLAZBkBEkEEIUA5odHMaPJMT+jpvt19wy+cc3Z8f5zfDT2hW/iZV1ClVXXrhvM7O6+w1/qudaX+Z7+YqVfQdIiKOCnRuWAAdLkl50xKiZwzACKCUgoRIat69287lHPe+wp77+x8Zv/PORouRrXp+xfR5EXbIUNKqW8jW5RSF/S5O7ac6d96dvJGdt/bTztj1FGe5c0LaefzO+3stClyif4v8bwUvbv++/vZoVHYQCPo2NCeP4sZ6JqoR0g2qJwgawSLkNBiQCCRLuhEKbUYdP9dePpmAkgRnzbA/ln/XF18L5klu3gJ8qILUYLo/kCFtDe5/QduMXOsXHwz9KLR/JRNFwQy+Eschqd+HnYOR/+zUhfvvwrpos87G/qxaRaHY689EeF0cRiFMMgd1BlT6IppVpAsNkdIlqw0PjmSLi54GVicFBaL4J5xEDuLqrDP+qz/wNM3ez9pUwOLTUqLQ7J4PYaM2D3ON0bI+cL2Vbz4YiXi07iyH+RCAulLnLb9LyxIYB+HXrz/tggXfa7ScI9NZK/dXiqB2EzMmpBLiqLGiHIE7SF7tBKQBBpS9CS1T4TsH/oO98kzb+YOxfj0wyD7Tmu6xMkvfLMzD6AXrXoxqZgiKcRdEaSU2icxFu2mi3OGs8/efwbKcPH5PeN7FxwOddHPRkYXfa7T9tNUGOxjrE5wWDKgSBgbBK0UORgsCkkajSHFQE4X6rq9BnsuUNQXdCRPEWtZpvt+2TtRnykFXfUbFyM5RYwSbGGQBCkLqlgmek+MEcmCRi+6Wkxa64u2X7qLS4buEoy5X0/u0P51ELl4/yrPLt6/BpH9a7/Tfv+8TAqtSyRGtBhMV3i6GrLLqBzxLqJMQkzYU1Q8/VQAsI/zdvjnArluqguMgv3f+zeeLob3kxFHzgmRQFEolkY1KysrDOoKpRQnH5/S0uKy6ye50OELpUVKFz85nbk451yaBJGnzGnf8qR48f5F6os+JysQQRZGHjtfC2kZw5ykNCRIGUyKI0xrKFXEp8w3vup6jqkJP/eHd/PQ4AQr4pB2iyiKTlVkY1HZURpPzi0+JJQGl0DqNdJ8wte/cI3v+qJrGCkDyZF14Fc/fob/+/cfIZgxy8yYBkHp87TFEWIwrLptJAVmZkQxHtNtPY6TyEuuOczXvPg4r3/uIY6PNEYsOnvIDRNlcPOSux7b4E9PrvMbH36IBzZqJkEoVoXh1NGpmpKWJmZqlclS0M7m/KfXX8GXPe843mbssKYU4WBZQ4K5gqlAvXWOB7YSZ9OAP/7YfbzzjnXu74YYSayGdbbNEl3QDK3GhgkhQ2tHhC6wZBOoSGOGzNrEP3xBwa0HMsXKZayqyHC8TLQB4xNL2mKy4dzMcToY7t2aceejp/jkvafwcoDURcq8TR5oTnuL0jW1a0iqIOUtrCqZDwaYGCNZRyQFBjpx8/El3nDzCW56zk18x1t/ly4IsRgxcY7xoEK6hulkE7s0RjIo1duyRnorrtSaowdXOX5omVKBYkgGVlZarLWEDD5FctaUxQCXIlZrgkRsWVEay8bJT/OSa9b4ntc9l9tuOs7xCkyckQN4a2myxSpLCQwLeOXVB3nl9Qd50+fdxPf/8qf53TsfZ2szEiURUqDQeXGF6a3D0WjEFVdcwYmjKzgBUVCQwDtImSVtqQX80hq3rEJI8PnXvJDvfCP8wV+e5Qff8RHuCocZxQn1YIB3nhgy1lpUhqqqCN02Ej0mlRyqB3zRC57LG24coQoYAAmwuVfrvudjFPSyKq4g8Tj3JMXP/+5d/PqH7+XxtErblQw0SNjC1onQ7Um2nDPK2KI3HJKDZpuqO8uVZceXXKv4b9/0eo4NGiZuwoGlg8zOb+KkY+ngCkUnhKxQugQEhSZ0LSo6Dg4rBmoxYnoBrEWRUlrcETVGF7TzDF2LhCnJKKYpo7PnJZcv811f9nK+8gXHudKCcVNIvS4qEtQuUGYomk2UaUBvQ9jmcAEbk1Mo6xnaTFmWaK1RSqEX+tP7funqukYifTsxQlzoT60hJyyRgfIUcZNBd4YKz1Hl+fsvWOaXvvtLeNWhDt1MSK4liiIZC0rjO4dRQkqRZC0pBVIz5UihWNGJpQwmzCn8FIktOgcqEhVQ5IwEj4gD47lBw7/5yufy49/6Oq47VBPFINowLEuaebcQ1XtyXTU+4GNAdImYqr9rpoZR6nj19fDD3/Tl3DKONNPHKVeG+GBw04TPEJKQ0OQsKASJARU9tRZ0DCgBvdhMYzWSMqhe3iujEVUxMgoJLVJVNMGRmg2+/jUv4iufP+6N0QDoAZgRW6nEKaAyeAHqIQ0QlYAumWW444l1Hm9gJuNd48l73zsRtEZrTYyRpmnA9INLoQOlcKZgbgxb2jARixNLcBrsELBELFoUN5SOX/6nL+e2a49Bs03MCTEVYFAZYgpkA3FYECuDyx1JHCiPTwFMAitkU9AlRZMU86yYRU00ZX8Bz5FWdeiwzasPwU9/w2289PCcxp/n3NwzLA89Rf8KRllDRhGVIemaGUOQGsSiYsMXHqv5te/6Cv7uT7ydO7YmjPVBusmEbtWSu7SrmJUWCtHYrKnKAhRo6TdSkSi02fUe5dgvsjU1Js9wORIRirLk8jLy2hccoQwd6JLsz0N5gE9M4I/uOM89D9xLjBHqJW5eWePm51/GSy+DtQQP3LMJncYUh2ldSWXniGgEIaZEloxSmhgis9mMNo2pTEaVBTM0731gm/vOdaxvT2mmM7QMufV5V/OKa+Fg56lMB6pCm4LlIvK/f/kreOCnfpMHfIsuKkxKDMqKNnQoowkhoGXhpRINaDoMwoDtFPmPv/4IZ6YTzocGJ5EVG7j1sgO85rnXcsvxQW9cNR6RGS85AN/8eSf4/t+9l8cnmug7MuoC48tkAVIkZghR0UQDqiQLmGCgaDhelPzKt7+Rf/JD7+TD58/ByoiyDXirEQUpRELKRNGgZPf+2F8hPImIkHbvhDH1EjikAMmjbcmkcawNLC+77hgnxoD3xJjRw5JzCf71z76H95xsEOn13yR3jNI51B/dwbVjeO0t13F6u2ObAXWeUuoN4oIbrbL4kPq7KQq1EE3KaGJqUapgI8Fbf/99/PnDW1As4eczBrrmJ/7iJCsy5xe/43V8zkqDLhxRDKms+YJr4KbLxpx8rEVURQ4ZrSD4gCkNetqilMJGQYcE2VDoXinNJ4H/9okzOBGakEmATpHf/sTD/MrHtnnp827gx790ja4cUShPdBO+5mVX8YvvvofzYY4aa+guvBoqbQoKq6hURudEjIEcWdwHLVshAp5rcuYXvv31fOFNQ4jnKMSgJaElLszzRMqZNkS2m4YkPVsqJRitqaoKrfe53wRQGZTClENy0ph2yvOvWKEA0JZoKpCSMzP4xIPrzFhC6yGESGUq5lVk25R8dNvylk+e4efvOcumGWNUg44nCSEQY+w5eSGKtNYYYyjLEkXv0pMElYJ5NGyGkqmMiOYAgUyT4Kw9xNf9+1/m4+0SUQ3QyaF8ZDXC5z7vaiodkZwwoog+gGgCUBaD/q6pMj55yA4FSADrPYFT6PAko3adQ6HlWFEjuuTjT67zCx/9S+57dAsUeCwUB4jA59xwGUfHli1vLnBlppRQXfAk14Cfo1NLgUckIBnOWlhWIwpvoGhZrQI/+nWv4utuuZnTNcTUkXNEG0G0QowG0XQxkXqH4p5yVuqCn40xZBIxZ+ZtoDQlqp1zbKkixwDKUiQPqeXQCBQaq0p0VkgAFTWFm3MoRg4HT9rcQHWeKlekbog3h/sNW/S74yGKMdJ1HW3b9i6GrKALaGBcjahsTeoCJsG2XaKWOcNwjlwv8YP//U/YiCCSKWyG1PDS516DzZG2m2OURmUoy5IuBZwSGpXwhcIViag9EVAmsDwumPsxrSzhRytsW2HdbyE6slpZysk2Hzi1RQxQhIRZeDJX12A228Lry5/iYk2oqDVG1zhTkaXgQLGMk0zWsJShUQ5sC7lGp8Bxs8lPfvVz+GcvGlHaCtU6TNQ4GdDmRCHbHKk0Ji0ME8BlS3v+LG0osNHRpV7W22SQZKh1RvwW82LMR9YVXhvo1iG2oEbMAnzNF76A5XyGjdARBjUqPolLq7Qm4/QmlZ6zJB6d58xSi68PkGMgJo+IRovpL/FJ0FqwBiJAjlD0vqzHfIkXRRIICpZCQzQl62rIZr3M6fObGBZSKwlkx4kxxDBitRjQhQkbWlGECYUfomOkFI0OggoKTdE7+JJQlZYDS0tIjBACxhi0rYmqYhoKXHGA++YDjMrANilGhsDmRmZmVjFs4lKmFI2YSNVNUbuObPb8sDuhmyLMqMi41hAcdKoi6CGqgO9744v4v151gpUycb4ocNlxwFiyG7Axy6D2nMgiYIxB1J7llXMmSQKVccGDsXQh8Ocf/UtmALGksWPoJlwpp/mXr1rj219zPTcfXSZlQ7BL2LFh5gzY48zTAebGsuU2OLg6IJ3fvKCvp7obi6LoN7PXERgPxbxh1UcOhczBJrCZhhgl1GmCabZIeoDX4NoEtKA1yoBW0LlApHeNam2p7WfgXdo+xZJqGOmANFNopxQ4huKowhaXFTOyEnKuUDoT28jGZEqIGRua3Wb67VMoyQvVlTMawSiF3vHV64aMorAlxkKXE42yxJw5GANv/qJr+J6veQWXWc+w0HSxj3GaqibtRgz6g2Ftb9YrFs58eusy5IQpLFkUqhry8Pomv//JCZPBCnUGZ8YEc4Tl0ZDv/aJj/Om3vYjv/9JrOG4cw9lphlY4Pxd8fZCQDQeWR2yfPU1l9G58c0cU7WysiFDXNTqEfnhK4yxsl5lzw8Dj48hDS4lsKtrWURlNReTAoCBlKKqi9zSgObsFRmUSmawMkGg7T+z2FvvZSJuSJCXzZOlUjVQrKFWRQ8SI8KrrlgFwqgAyqtI8fHqDejiikrjLdEkgivRcn9kLfirVx+cE8GqNhOXs9pw2QS0KS0DrANkTJfCPXrTCf/6yWzg4PUcQhTMd0U1R7PltFVCXdncx1U7cTwWyzsTcXx1CFuYy4Mfe/m7+5FFofEZLfx1ENJkhIyN86+1Huev7Pp9/95WfywtXGy5baZGwQZUENVeU5YA8Li7gzN2A9oKstZjFPZOkEKDwMHSKYWsZdDV1nlOUAzYmAWLgf/uyV3EEIDu6ZMm65qP3PkrwHWhDQlFYTZKCQi4e/gKYMWIzVMxSjVNLNEExnc5ZWxryBbfdyo1HVyhzRyktUSyfnsB95wOT2bznxLzDLoqMQiGp55YcQXYm3H/KIaQMj3ctb/6Jt3PfNpQYyB1JZTyGMnV89QsO8kv/8qt4/mqgyo6s+vfUHmNSliWQkEUoS2UIwWGMwQVPYS0pJYIueGiS+Lc/+zuc3GqZhUgMLSEEvAhhwd0qd/yjlx/iF7/zS/gPX/08XrK0Rem2cBi8qtieN88YHNhvAcYUyLn3bAgwqirEO1SMDKoa62aM0ozr1kpe/bwruf0aBbGB5NDasAF8+IHTZEAbg48BnSPJWOxnEAsdzs9wmAnHzZSj6Uku605y6/KUr3/ZFfzrrzrey89uE3LDJvD9v/EpTschVV0yjwphBwigQBUYSZldv1vK/WIvOqsTKJV41Bl+5Z4Zd//XD/HD//A2nnfUUBmh7CK5LIE5L1uz/Ow3v5J/87YPE9qWpDwauxCpUBjbM0FKKNVzhBJFCIHCljjvqE2JdwG9dIRPnz7NbT/wZ3zvlz6Xb3rZAQ4NhTnQAqI0RQJDxzEJvOmaMa/89i/nX/zqR3n7x0+iB2us2VW827pg8XoR2xtfTdOgZQmMJgv4mDi8phg/tsm5ZhMvA66saq5aE77yNS/i8194mAogGZD+uvanJy0ffHiTKL27UCEQOkIe4nxDz/rPTj/z5i+haTvmXUupNdccO8DVBzVjSejcEaVEV2tszzve+sGHeOcdp5BiwJJEOlNBnPfzEk3WeiHBdk3c/dCLhFJT8ENstUSsDnLHucQ3veV3+JHvfgO3rcIwtzg0mQFlu8FN41X+8z99KY88Pl1w4YIbMhij9hkiCkkZbQu6rqNWmq6bI4CExNbEYVeO4k3Jv/itT/Fj70686RXP4e/ddhU3rBqsgWCWiMlR5gJVJq408M/f+GI2Jhu874FNNtUaQ72HF9qvL8kwn8/pZJUChcTMOHX8p6+9lfyGW3vRnsCPYAhU/dlH2gTaEK3lDPDWd32Iu885BhZUdFSFQnmHqipy2BfLfRZ69ZUlyvQX/xATKnms9CqMGECXPCCGX/3Ak7zlHe9lsHYzoZmxdeZx6sPX9f4A6F2k2qCeETaxoEgHRuO3YTl3ZJW5jxX+3r/9Bd7x509CpdFtoIxAsQqSOJC2eNkVJSqXew1J3mdV7gWEvfcYY2jnDSujIbnrWF5aQhnL3Gdk8gRrQ8tMj/ix9z7K6/7jH/Ctb/swn3x0CxMbCinotGVqSiKJW8bwT25/AQdNpKuKxQHdm99+I6htW8LuowBhkzFwsG45UkcODuGY32YpTRA3wySPVg3kGdsB/v0v/hkfvv8UebBGWdeE4JCc8d2s9xBdWmViDRAcGii0QtmSICVejXB2hfPrZ/iet3yQH/jdu3GDYzSbZyjinMuPrNFNN/cayoISg6qjkAV0Ak9iUtBHOlNGpUNk8RxKZ5mqmqy2KZNmuzrKt/zWXfzIu06yUZn+2IYtoKMzQ5oIyqXesPKCSsL25BzBBpyt8QxQUqBV7KMZKuObCVVVcnZ7G20Ug9wwSIHsHU4UM2U5Xy3zK3ef5e++9f28+Y+mJAFDogaMyxi2+fwbDjErE0sLREShC2L0pBR6H6npIz1JFQyT6hWMFtCWMVNEFb0oCef7vyNYCWSl2SiG/NpDBX//v7yXt39wnU6NWU4zooMiG1LUqGqNuj2LWEMiUpYWnyIp9dZvXhzunCPrnafbmrMZ4WObsDUD4xI2zCkcZKt58PEncHpEGyylLRARttqOUJVAQYrSq0ybuUCoy46XZs8GAi703uw/6T/43nuZO8+3vOZ6lkYVLQXj1Dc+tTNqanaAE4PBoDd2mkjIAZHegxuJRDRlXTNtPMOlNSazKbWtiPWQ1AVKJxRdxGrBp4IHz0z4pT//FFfWV/Fdt19NVICBQAkl3LxykA+cn6F34JciKFHknIgxoncgLBpIffRLmxVyNkwSWK0Y6AN0qncsnNz0fODTj/BHdzzBnz6wzaluCAdfxPL0wUtyXwjhgjXcWb2cM9f8P+8jTc9Sa42xAw7Zlp/57r/DCwcVhoaV1TX+j6+4ne/9pQ/ihoeopKOZdUg5YiABKPoJLABvZgfnkARkEXPsL9J7G6ifgqXZ2dDH/TI//Gd38tDmE/zAN7yaAwvRMo8JZUsSgo4JBGIIRB/QusSKJrmOTEa06n2glDTRUcSWpULTdR3LsWTeNqRSCDbitWJUjtCNIU/gR99zJ9/2kqtR5RykwGGxGq4drvD+809eMNYdXRljRNG7E4kL5rP9Jv6Pj57kv/zBx1kPoMXRuMP4rkNLxGjpfb1JsVZ1pNkD+6yCZyaR/h2zwPL249l7ruIac5MwXcTIAe6ZrfO2376bF371TaAUWeBNLzvMb77X8J71gJOMqkZ0WbOUZ3j2VElOESUiJNnzX4Yd3Mo+c94srLKnArZWKmFzdJS33dXwXT/1IR7bAhTUNlFm1U+2qECDVRpiwohgFyjAjKGyBfVgREBYrhRr7gyvuKrmOBtMzQxfRExVUJYDUtRsto4NFdkYKpYHNaoAJRGImN4lzdxNMXIhvuiCIK5SFMVCnPb+C2oFoRtw6jSsbw1Z3x4zE8s2hu1csc2ARgZ4bRBJSLq0U0BEFtb7fsm292wlPMHlds6KaWm6Texwjd/80L3cPYEgJQUwoOG7v+bzWE0bBFXgIlQq0bYOVGYn6pVDRLEbE1NkUYtI/A44q+/YWvuM+NKQNhnOIgN1hN946Cxf9xO/wqc3IxIN2js62BXZpS1QOeGd6+N8WiBrXNvhuoaumbAsU77h82/gh77pJfz4t72RW47WHK4Sbjqh85myGlNqwzB2HMxTvveLbyVoeikSIgUdsYM7ts9h0uBZYYo7Qerd6Hk3Ry8gHMkUmMESpigptx5hJW0x1h1GIgFhnjRTSny1+hlt5s73vcjN3rNu6TCtW2LbLDEtayQaZrnkp//gIyRAsie2ns+9asg/fvVN5HZOqRV1nKEGy0R6pF5/n4+YSCajSDmDgHNhgQBjV8Dv58y8D/GmKaDbQtyEYm2V928lvva//g5v+QdfxO3Hq10h5NtMDhGrND72kXhbaAoshBZjDEVhkG6T195ylOtU4rLLNX/45lfx0AZ8+LEpn3psnSfPnKNwjhefuJ5XvvA6bjoCmRakWNxnhTtPee5vMjb4Pji+GPMO/HKHS2KMeAfWAqXGCUy14ywNXuYgc4ajy8k545xDE6mrPm0jdnNMzLhLoet21mxhPUN/fnbWNbUT2mlDGtfUBQxdR5syv/3BT/EPXvliXnoMVLWEzo5vfu11/PFdT/Kpx84jOpPtCqSuh7BmUDHvu2fmvlMX/AXYTACt1d7A9o/UW/S4RlJL3pxhi0PcHSLf8JPv4C3/+Kt4yfUFCFi7yN1QisJoYu7jbyUFMQW6aIimYDgcct3xNSRNGcoI8oybliw33TiCm0bA1b3+TQFtPWChEah758QDjePnP/Qo87jEst6GRXrDLr5V9rikaRpSAb0F5LBSspwsl+Uxfl4xiIHHjaMsS+qqJkZPdJ4uJLIqKGyJRH/Rjex9w5oUI/P5/GnP1+oJG0cqpjHSTE5DNaYb1bh8hP/z3Z/gHW+6FaPBiOKIge94w/P55z/5e2yqFZrJNmVdLuaXF4iOvOeIjjuA43yhblTPEgCYZA/ZElNFVxboBKOZ5SxHecPbfo/3f/iDzOeuF2U572Jxdi0736JVr1PEVpy46lqKRdcuAXkIuiDbRNSOqB1ZJ5Q1pGjZAkglAWh94AMPrvPf33cfEseYcvqMCUHQW5ht27Kbt6MyM8lslopzhfCESpxeLGJsW1I3RYeOksiwECqtSeHiGwnsIitSSjjn9hTm4u/L6QCbbUmcVVyuD6JcZphrytmYD911njsffAINNF3AEHjN9cu89vYXsp1qlgfF4oD2qD6VQW3iEVswiAXLboNltU3WFuZnIZ1BgiUaOJrP4ruOQiKDYJmrwFj3pzyjMCliZUZSE0iZQTvi635jwg/9xWk2ATMesJw3mdgaLzWrHhrraAcVyniq7YeZbW/xa5/Y4rF2hFU9ftWxQPcFhw4tkjokB8iR5XZGHsDDwH/46Ix/9at3M8+Ow0WDTA8SEpTRI6kko+mMIvvzrKV1zqkre7HkPaSSAmEUSg6ElmGekAvp3ddKkcQSxOIx+EUsVl/ckAVgoAxFnDKKT3IOw0Q0RQqQE4HM49EyFiikYZOM1jUS5lA4chL+1a9/jIeAoqzANyyJ5xs/90peMD5Lh6fMioCQk2Y6rDHGGpxzdBnq8RHOmyOcBJYHB6mANsJDE0j1GjEY5ikRVcAXULcXn4zoVX7qd97L5uZ13HjzrSQG6HkLhWYLT5VGTDYdelAig8N85LFNPv1z7+RnDg150Q1X840vPcGhlSGHlyzWDiAnwJAFsoGTZshv/enD/OYHPskdZ+dsMqBcWSYoTesS1qje/xF7B0YMMCiHFGbAmVnLXAqGts94e2KSeHLrDIijEJCciJQXn+AlaNoapDpANgM+cUb4+Y9MSNvrbLuOuerXPYSw65l6arrDx9fnfP+vP8DNR0YcGxk2J9s8EWpk5Vrik1Mse3dYpRRSfu8fZEVB6Q1TFTmsNjhsK7KbMi0clRqhx4f55COnGC6tUYcMXUNSscfoXGwyYUhlpxRsMioqZluZwfJh1icb2LFh6AYkLURjmAeHiw6jMopAoaDaXufQgWWuPLrGiWOHWVtZJiV44sw5Tj15mvsfXWcaE76oCNUAF/vsMKuEQmli6jBZoynJKLrkIHfQzTk0HrIeS6zfYEl7KCtmDNloE3VRQDvF6IvP71JkZYVgYeamjFRiyUS6ZsYETVvUjCJPS3zan2vaqo5SG0zqkXfdvKUYLBPRtDFSSsLrFisDvA7I8vf9SY5toAiWWV1TuA10qnp8qXJEl8Ba5q6jKAoMGS0ZUxr87OIpaUnXRC20sYGupUqZlfES221Lq4QyhQt0qBKDUoaYIWeh0RGVAyoHDInCmEXILNO5QJHGWKuJOdCFDqWF0liyjwTn8NZjpIemaN1DSpOKpBTIsSPXR1B+gpaGLmQ6NUQXY6oMNFOMvvj8LkVFVHQkosmQAiaC6JLOFnTGMpz3UZ09hMeFnJltImXBRyFlRWkLkusgOCorpFzgdYuhxuuAUWQkR3RMqCgoSWTpdVXOmmAymshyXZCC7zGueeduenHFUcgW01lkdXwIXY3Ynq6z6bawxQA1T5hR3Q8ueiptScnTdfPeWqxqUnAURqHEEHyHbxuiUuiiZKkomTshpEiKHToFFOCDg2zQZYEtS3LO+Db2nhhjIGR0UeCtxU7OYFSksKBzRpotihApc6ZG2Lro7C5NyXhicFhjMCiU6zHE4hqUm6GUvgCUdWECkmCSpm1bBoVBTCInT1SeclwSQkDCvtgsERN9wOSEyiDJL5JyPNF1uDIRTX8dGUSNCpGYMypnqi7TlhffzNBtc6BaJc4bZq5FDzSoRPSecTmiazxWD4i5ow0RbQr0uMYpxUY7Z5wHpCAkQMkAXfYGeAwR10WoZgQSovrrk9aGlAUXEhGQedfjrgSUFqwRUheIIYJRuNFRXAw0OVCUisIGShHoHL2jF+kAAB7GSURBVKFv4f/TZna2xpPILAaR+oCGNVAZaPNeyv5+nNLO37wvUJKRlMjJ7fqxpyETk2XIhb5ys+v9UZC1IWWFkh4cdKQxzCwkLeQuErWmqSApIWeNuIunrFl7gEkXSLGjHlbk3Iu/nDNRPJlATAmUUJUjYkrMmhYpDXU1oJ43oBQimpSFHPt4qxVBbGaaHVr3d0nnei4wRY8g975jqAfMk0eXhhgcMXao5NHaEHKibO5Hm4oQFUYPmPvMVGmCMqiypnIXz5+8FMUuo8VC9EStSNrgAY1f3FH1BXoSnpoHmyjqiqad4lPG2rKfr0uM6gGh6/rNRxbZ46pP51BiiKrAR4ugEK2ZFzDVCWUtlfOUxmLxZJ8YhERziZz9aVNix2NicrQpYrvMyNR0RJrckasWLQWpy3gX0UkxjInCRVLXMdcdCcjZ9GNaFJQQMpIyNZauDT0aToZEFdGxB3PXCCH0WWDaWnJMxJgxkjAkok9M62WsLujmHcOsEBpsjmiVyEkBl/bwXIyGZLTuUQxJJcSa3v2oI84HzFPS9PdzZc4ZI3O8F5JWVKMVYuuxaFRsUFsb5HKvQIhSCjn4nb+XgzUkcfjeJ0PIFYPQ4Z7NW/BZ+htBWQUkFbQGyjhHJVH7E6Sf0aH+WfrbQSqj+toFaeF/XeTNp88AKvhZ+ptFJu8rorBfqD41dvlZ+ptPKqs+iCxZkEU4BS6oTfFZ+ltCKiu9qyd30xJIpEuUAvss/c2jXmeKQmUFKCTnBR7os6z5t41U3ndX3BWxn7Vo/1aSqd0WE1WTdCYQ0UozSo5WBhR51oOylCaj+kBojqjk+/JecpBObdPEiDUHIEe0TKmKmulE01aeY+fvpfuLX2e8/QjVyhLnyxXkwLVIfRA1XiVUI8LgIMku9XEtSSTtiCZhuorOelyV8UnAGZapGeaIn27h6ouXK/vrpqzSopgUkPvkI5X3kHhB+gD20yqXLZhFidn9/al+2c+IYkGyjioY0IaLJkMknXs4iUBEkbCI0gsXmzDrtliVMSt4cjxLYzsmTtP4koFe5tCT7+TMR9/D2tZD1DS056foaoo7t46RgraYY5zF6lXyoasJ19xCe/R6OnUI5y3jdAaaitoPGGpHSueJbLOlRqThIYrPACH310l9euKiCsci+JAk0xdAVERVPPOLO6CutIfuv6B06/+kZLzoZu4OJmU0AZVyXwmSPpxUrCrchqcWg1PC1GvKco1R28KZP6f70P9gvHGG1YGQUyKESBXmDLsNBjnRtgUimazWcY+epDl7Dxx9PoOrXoS+7Do2qkPUTUfZTBHJhHqFRgsheUyaXGr4f+0UY9zbBJUgq0XtxQUqbx/HPdX3+tSNe2ot2aemIF6Kcs6X2MxUoSViSb1zWLnFi20f7A2BjpZtPSbIGgWWlXZKvv8Pmd33DsrpAT7n5V/I2dMPcfLUI6hxhXOeQQnRt0QzRtkE0mDaLUab26T2LLl5mHz2KObqr8COK+KSouksEoeYZDBpEy0N/lIFa/+6Kau+nJ3uEeWJHkDeb2hisN/v8tR9yRAX2N79G/4/Y3juvH/R1RjEPqISSfRaMhMF8mLAekuztFzho6eYNqzGKeH+P2F+z3t57soBXvbFb+b40ZJ3vfPnmLWeuqppUwJTMgdS3qLqMkUSoi5ohopOO9TZ+ylOP0B9Zh254aW0J17MfDjCdC1l1/anuFyF0P2VJ/6/kowxvWglk4lkIvRBoD4mnNSzcmLOuYe1LJAGz1Qo8q9Cl+TMIrYEhKA0XgqiFkT30D6jhFXvYGuDanKKfPp+ujMfJKx/mhuPvpTXverbGR1b5q473su59ScYmIjppqQIKIUqSsqQSFozE4NbHBYdU58tDPD4x5H5BurMGcxVt9IdvYpuVKIaT+GmIBevivnXTQlZ+D97/alQPS62d8P0RT94OkR1Z8NSiheI2WcTuZ8pXXQzGxxZ95WhCxI6tpTNNoXbxIQ55x64E7f+BMNui8KdQeImN998G7e//GsZ1Cu8+50/zb33fZTgzjKoCyQpVFS4tkM0qDRgZhJznSkls5wUdYaQNW32fWGksw9QbZ2m3n6M2ezlzI/fSNaDPoPZLP+VJ/y/knzq6wpoeu+ZIqNi7LPocqQq9tWrf1oREGHuZLe23zPp0M+US3dDZhf7UDuokazQ0SPzTeTcSdKpTxGe/DRx+3FGw4ZxZ6jbSLJDDjzn9XzO572JELb4qbe+GTc/iy4zqha2u4DJNQNVU2tLzg1RWlYTHEmCl0wjmfO5QOUhRS5R+jH8UJNIpJN3Up58lMHVL6F73qs4f/Q6imcAFv//SaL6al+SU19zMES868jOkUKgyOd6SEe8kAN3NtAPrwHY/T19JkmdFyFjUqIoFPMMRgLRCaIqytixlZcYyjbDyYOY+z+Iuu9jDOfrmCrSFJFtL0zqNczxG7nixC28+pVfQPbb/NavvQ26ObmucL7BZsVAAzmx1c45eOhKXvu6v8O7fvMnmEynDAY1ITqC9wxrIcY5MMfLmOwdKgVKa/B5SvvY+7CTu7nmwGFO3vBGbD3ElEsEKQgooiiyJGIOjDpFVAmvAlkCQsZkocoajXB+R0xLWhRs7LOIdH9xpMDhHGQGZFXgM4jJKBvwqWPVKeZUBB+wpz7K4N53MVz/FKIMjV2hoMUlRUh9cLy2mpxcn9OTYawMXH0bcuNrmSxfhUModaTQCedarC8Q1VeyjFmIOZBxKPEonZmZATYovGlYSg1GsYW0MPCRoiiYaYOzgoTAgcKjzjxEd+e7Uac/ycjMaa1G7BVcft2tfOn1N1GP1lg+eBn1YImu2+KTH3kfTz75IOgZzBWD2pBxuBgIUTh82Y3cdtuXcfzyl3Hs2Ds4efIk3nuUFowxhBBwzvW1W60hKtODoKQHJRMb2m0hBMfqEz9CPnwCf/nzCYeupxscRlFiu8ggetwoYb1QBsFk0xdA1ooNnXGSOBym9AaeJmZNwpIoCGgSimmlEWkppaMURxn6AlHZKVTQxOhZPft+ikffj3vsLlzTwuAY1gjMt5nnhJXIUAV0bei8J+Yey1sojfjzzJ64i1gdRN+wTB6dYMMHCjdlrAxtVZKyImYNyoIoREHOPcKwjB0qCSKGlDUmmEwuR5RtHwprVERnz5JrKJ/4GPN7P8Lo7J0M4zqz6KjWbuIlL/tqXnzr67CTJ5k0c+abm2ydP82jj97NnXe8j67ZZFAllmvN+a1N7HAJXa5x/KobuP3213Psshtp53sWXoyxF1lKkXPcFUU+LdImyKjUc0wpgotz4rTBsEF46FHsY3extnIV+shziEevpVs9jhsuk1tLqxSxsIsKkBGTPXXyjEnoNFr0l9AEsngiW7v4m+WuYBoCXmmCsRRKUXYNxWSOnUzI97+D0GzA9ElKv8WJ1RGHDo3oQubUesvZOawNEweHCVtU3H9mwjRYsqf/Nx26oGrWcY99ECeCufJ2ytWrELPE3IGUkRRjn84QHZL7YIjSGaM0KQo6G7IkPAYzlzGTXLAdMuNS0GWknp5n8ND92AfeTt44R2UTjgGqPMANNz6fcZV519t/ko1TH+H85iZtcFRVgdZC9C1KMsnDpjvLytoJRqvX8+Lbvpgrr7oBrRXb5x/mT/74XayfOrULgcy5L1horcbavfuXLBJ/d6qUFFowAilnKnWArDwpbeHO/yXzzXtoHzyIrFxBuXyc7rqXUpRLSLWCl5IuaVyOBDFYndgwLQrBZMGgMQg6F5jForW+pTQVAw3KbZPXHyQ/didy6m6K7XUGasp2rsmm5viRw9xySLFSzLl/veXh2ZRCL3PZ6oATK30h4ZMbM5QUfcJvimzpJSrpqLceQWYb6K3HMc95Bf7gjUyloo2CSrnHDFtBpUSOjhwcMWcwyz0IT2WyqpHxd/9Wno0Oc2AyZxQ2mIUzqCfuwXziYxwKDzJPmU4XKLHY1HNv0ImgYCBTfAgoa/C+F0FWWZKH8WDMlTe+gMuvuIGrrrmV+Uzx4EP38/D9H+fxk38JaQvn+5oGZVnifLu7mTtFfzF9VS/JGQkJcuyTmFSv59I8k4xBCkNUfX3X4D2lCJU1eH0Af+g44cSNtJddjxsdI7GEaRXGQyyE/l/wRJBIxvffc+9THZw7Tdg4TTp9P+bcScrmSUzcwCqPtcLUL3O48Fx/AK5cG4JYHj7fcP+ZhvNzGBaaF193gOMriU0XeM/dp5nJIcRnhtGxbQq0JExq0H5O1Jp2dDn++IvgyE1w/HlEH3qwprZkW+BFaBGCUgzaCaSKmRHGpSBXf8uPZu81xSMPkU4/wHY4Q5EmFOsnMbKCXR7Q+ZY8nbNqS4xVnKNlohPLYUBKgaIwdL6/zFvbV+I6cuQYK0c+B2OF++//NLPpeVRumW2fY1Cqvhyq2pdyt/iHMUrtVWkMOaGVxew4pFMiZ79roOTRAVLToNqmL4tdFHgtTEnMc6KULaIvEFbQg8sxh64kHb4Mt7yCKw0prSDBoboparqBnp1DzzeRZoIER5o/ACGiXaTWFlMaOjxN9niVuGFQcmIJTiwXUI6465ziE49OmDQdK8OS6wYdzzlxEK0ij5zd5OMntwjmACoJZQ4gHq8rPAaLo/DbOOdo7TJ5fJSpPUg5HDNaPYxZOURbLDEtl/CjNczSClW7jU8V5zIMjEHWnv+52c8Dy+02ozQlaI2uDKmZIvVh2m6GlkipM9Ev0gmKCowleSHGsFs/XZneKIj0xecjqwQ3xSiP6zYoK0NdD5lOGrSt0ewrhaZ20hT2PCIhOPp6rQWyuEX1peECRiViV6N0ROs5KXcLR3cFjMiU1MUMHxIhJLJolC1AaUKKuBhYdYFIJklvBe94t6L05e9X43mwQ7wa4BJk76hyw9FlzRUHx1wxjig7YJIMj20GHjnfcXZrDimyVBlec7XCDlc4M/Hc98QWT0496HqRRZbJ3Ryx5SJ5H6rSUuDxXbcwChcpG7qgxbKdS7pqmcHRK1k5doLm0A3Y+jDrpkfu62tHxb8WN6Eu5xgT8U7QeUSSIaQNTO6Nk8YYpoUlWEvphXK7xVVTiqL/rwTWluQshCQU5YjWZZQ8js4zRqWmtiWhC7iYyVYTdQafd7OydwpK9f/HpK8FUJWWjJCygLJkEbIssrqVIuc5SiuS1MRUk6XGimBUi0obRFeTEZQWsvj/t7sz6ZEsu+r479zhvReRGZk1dFXX0KNpy1jCyEggCwlLiA1ig1iyYMuGT8GGNV8BvgArL1ghJCQjuRGTwTJgl92urq6qzqqcY3jv3nuOF/dFRtbQlZjsUiGfTWRGxst47w7nnvH/R0sPaUnMFbA/tYJGA2+IV6JTglNaUVopqLQj28MSkURswNSzWniOTxsezI+RtsOJcbz/hMP9PdDMbBq5Npvyq7srFtlxf3/g0/0VEju8k9rFLUKOu7ROmZRTnGUGGk6sJUug6xo0BNQUKZkJiZkNTPtj/NEj5PFP+GTR0YQpMp2BD8hH771na8d13VW8NjyybljvzncrrXdTGOmjRKQO9JgukxEnofWePid0/D+Ir//HO7yL9HlOcJ6AYaUg6hHXYBIrwUzUZ7ukqF1e6/tVu1xjz2XFW8Z8IOMrrkJZ0pQVt3Zb3r9zgzKZ8PjRPo/2DhlKQH03tiuABCFcQOx2kRQzBmm4cvcjtq/dIhRbd02Xc0Fhh/Mex+a987KuoC5+eo4OsGLQyXpSMVZDJsYtQvRoSZiVCkyhRkkrunAVsWp4iGXUK+YLOkKXtirIOqSVS+05GTEVTNOF2HSvW2xs58d5xDtCmNBYhOhZFce9H91nuejpk8PHQPC+ehmWL+Qp+9/IxBu5X3D66Gcsj/YJLrwI5WnUvlb3kjaz9WeBao26uAGBMEOttrXHEMgywZyQy4BmJZBH16KgDDR+IOdE1jRitvsxihNQ5wiudm/lnHEIbfT4JlSA/5x/EVqx1yIVH8lIpWDFyCIkLQzDwPFi4HiZMfM4F1AJUApiRnQ1IF8u+QTewVYUcjqm5FPC+SToC0HhM2ot4/lAsJkx8R7navQfzVUdm0IBZ4nGC2m1QvKKLa9c2Wq5fW2bd29d58b1a5hfocVYDIW9/TmfPDziwdMFJTu6dpvT1ec0IdJ0VQOkAkMxRGqZxBka5xuSiilQmQMLgojDQktfBtKgSLNDYBMYMc2AEvB4kUtP5rxPFTcXrfHzD9//ip3feWct1SKUtGG1eVkfoUlEtO4SJyPDD3YGMRatp3Fw/cqUD27f5J0bu2w3HtKiJqfdguAbYrdNoeXzo56fPDjg3sOnHBz3xN2datUqmDgEX3OpBZIaE/eLUyJ+meItYSPmgckI4LsG3yhKSkpw1QATLaxhKJ2N5K2XJJZLeLwDKRm0IB998L7BZvetjZ1SCt5vwJzWN3meMneVrNIwoZSSEDNKGhArdF3HB92c9+7e4Z27bzONnrQ8htTThUqdCB3DsEItE5oGaVvmKfPk6JSDk1Pu7ztOThcczZesklTgjDABF5EQcenkUoNxWXHUxLRzjmIVWs3kPE1ypCJ62kgZUlNlqrUz/AzK+f8o1kzRnLEyMlh87f13bU3xu0auqj5eRmJ3jr9rszPXu3dic65e3WXadkjFvcBKoo2Bqzu7fO12u1mFWiqOa3RQKpRK0Y4mgHcV5TFrojgQF1AviG3Tp8LJKrN3tOSTx4c83DtmME873SKVN2vN4hyaM85B8OdQuFytQPBugtpQI0pjHWsxhxCr/6yX1yxprEPysSFsT6csFgtyzkTvmc1mTCYTnHM82Nt/oS5lvXu99/zBNz9kMmkRaj6vCx4tmZIGmlZIw4Ji1fJ1waMqzIfakRzaKZBJKFnrcnWuofENap6yMnbcU5oCW+2Et+5e463dbX402eP+5wecLo6g3br0YFxKnKdYwukGK8nMEM2oGrksayA/GN7VM9Jy9Ry88xfw3l4sja3wOMx7TBPyF3/yu9akQwYi6iOdNxxKloiKsVhmDo6XHJysSCmx3XneurrN7s6E9gLm1kuLLXHiwQdyEVQajMDDx0+4d++n/HC4Qh7rktTXHGWjnjgoLhWOrwyV5DU5GmkRZ4gmRBLeKZJ2CWOVQB8K85joGx19WiHe7iqFx3j0rDXY2sLe3huPnFJdKD+CKq7dp9Ox4KxiylfvwOtYogkMF6hZO4dizai6CxubZWYtrvQcxZ4kELz3BAuYxJo7lGo+V/2e2Zo2TLevcNsqnjqWCdJX8pTX7Bu4dfWaSu2ctoJvGu7evsnulRlXPj3hcD7nyWLJce5J0oA3cqhI8ztHV8ApFgslJFbOGHzEwgzXTOh3HxNKoA0tfrZDc32LyU7ENRmHMRePus0ZKIzumipelUaqlZ2HQu4Hlv1AXmV0pK26eVqRPPOQyEOiHwbykNCUUVWm6dU4Q9PYVOpGVbTUIlfnHDLe0xOZE0Oi36psiWGtMlX8C/ipa5a9oj0iAedt5LzyOKfoaz6yhDjW2dQbKSXRzxM+BrYmnm99EDj2N3jY7PBw+jaPtu9wMLvDor3J0GwzpJ5gPWE4xC0e0fSfEdgju0OK74n5Bp0LdK4hS+CUwpIlhEzjFVnFF0jJ3bnJXbiKeeCoR6LXFk9L5SV39GNgQACvRrBn2ybTveNXPv/B4+Nny01CoGkaYtPgm8hMDK+JyZZHT6yq2W1ZkF3LYEKwhFihuAZHj6nHxIFVlSpOa8ZfMtgXVGx/SSKEM6PCR3cWqbKxuSkMCedrRYIhY56wqrJAYLV7xCkznuhbPJW7zKd3Ob36Ngc7HYfRWBx+B786xA8HeJ+QtkWlARVCKRSpq/VlBVlQF9ea1kMAsco1IuNkzpt8tqNhY7ye9fT4C3amTlDsTL2vFeH6+52BLyuYNPgjatCgcrC9iOsjUksr/RltDTVAPA7o6+4Tc+OZo6oVozY4ol/HhjNpt0ELaBnwCo0LVCgLgzyQDm4w8Yn3/AN+hZ/hVxNWJ9d5FK6wR8v3f/vP6A/+g37/u5B+Cn1fd9XgcBn63XN4R+PDmm2Ol0CsEKHnwqAYGG48pnREm+RMu5x91qCUV9f99law9XVONuHS8e+nloEeCcqsbQhrtXEGQULFM690vbFWapvCui/CCYZgEl77ZPZpQdM0iAC5IoSY42ylbg8elRpHGcQoPjN4RX1VGjePn5JCYIiVs7MMJ0g65Up3n6YN/HPzl6S2J3f3KPYQK6eIy3RbLSF43OpcV/k5dXvmk4faU6JWK9qLrhmVavbHtXEskubsWmyjsrcuqPs1v0EeKeh4LSNoLXSxQZ0xBAfBVwPoZYaMmZGtwnQ6V+OAAJlKFF4u7/NeKEUH1ARxlfPybEBzPUdPKTS+qfycBlbS2P9RB7tvIzkqvRd6v81Rc4tPd77GT25/g8c3v8Kk/x798l9Ji//B23FV19KysMwiL9i26eas5NnskSAMefQrTUaCVYe4zS71i+FsN9X3BHObn09c4VXirX6TMTIKstmZIkI3GIojD4YvVg0gRsdeAaPuRKWmqkyp6aix+lqdR7zHBwfD67WAuq4SmqXUgxpN0yFqqBreO1oJlFRY9ePZ5T34SJYa/P6XG3/I3lu3eHT3I/rdd4iD0ex9hhzeY/bDv+NJ+zFx+Iyd8jmNdxTdZZ47TBQfWlZtf6YFno+AOVeJdqq4Sv5KYY3fqwJusv77WME+/rZm/g3l1TszlnyWXhTZsC+s0bjDYFhweFdwRQhJClG08hWbYN6PlLkeXzYrR91oANUnQobLB7mNWAuIx2WkAipCcrXLzOm8rupQDYtBh0pUHgR1SilzOvP0PrIMge2S2RqEe7N3+be3PuLJ1/+U1fHf09/7a6I7IHHKKs5ZLRd04RqTobYE9v5axZM3EL+seX8F1OPWZB5rsvj1zT+/qZ7TUn40gp5577lXLkB0yc/Ebs99wXi+5egJayMtvOGeuOLnrN1Vrw5vjqiOLmcwx8LHs9wpUuOZZoapUbLR+Y4ImDb0BAZXiczNCZ0V7oWW7Xib0FzHTu4h7oRWpjjfYfoUZPomH/9Llzc6ma5qf4xAFocgVM70Wl0+c7UyTQsUk1r76ioGAwiHTWCrV7waLmciA11K3Dl6jFs5Prn7V3TtrzPc+SOe7s8o839AV3vEMIEwO3N7flnE/95vfPjn0QbEwtjyXivfnPmNQ/SaJJQI1mC0FBdIzpOckLySvbGgqVDarEGmCk4SwZZ4eoJts5OgVbDgyD5R/JIiNXf49uPvM9eWx7e/weH1X2Or+4BwnHDlgN5/jrPLYeO9cTGHEyU7o9X4Znfmum9JyHhTvNVghFAAJQ4B8WOO0MWK8mnCMMZJh3BMIeGKZ7AOxYFvaCXyljbsha+y9NdJ7S2ayTU6dZTmu+hSkWHyphuvv3R5o4/Txx5vSrCCX3dSmcPwwFiNp6CmmGpVtQLO16r3kBOrsMV+9zYP26/wYPYuR7s3WM7uoPE2V6aBJZFJAn/wXyyP/5Y+/RMdS6b6DiuevMnH/9LlDa/NTZ2s4hhcS++2WMk2g2u5NfyAIW5xEq+yH2/webzFYXeL5eQGpd1hdfebDE0iS4OkDs2Ghjm0EPyE+2UOTz4mPPkefvmQvj8ixA71CZUHcElA/f9v8kYnM6YJjoy6THKBk7DFQbzOfnONud/mb278Mc10SrO9A+2MITT0RHKoWZ52EUhyCjqnO3lMs3+fYfkD+vKArIlJ+xhdLtDylPZKYSWCutsMOUI8Bv0lm8zBWrAV05DpaEi5VshZA6SX5yvXEQ21VxO1iE1RPcWzJPgJKtsM5kluhTVL/v3quxzxVfZnv8Xy5tfpZxOKLGnVmLkObQODFkrJBOuJZYFbnLA6PqJfHJNX3wFWKHPmssBYBzE8SMBygQgSZ6xW4z4si5qBsR3MNhjpLxNXXl0JcFG7+stAsp6JBl0SPMuFOSemmDY1aBDJtGJY6smlIBLx3iqf+Bd91/h+vCBVnmwfa7cotoMMPUNZ8nB2h/9+//e5f/ubdOF9ep9YBaO0Hm082BXmqeYn3957yGp1wPz0ZwyrnyL2KV4e4+UQJ0vEPkBkQOipgcZNBAYKRV7tepy//5cNbL4wKWRng/Gy6y+qJLhoMi+cbDWyc5WiynmCmBJjRDVTTGmCR7yRbUDEvbDyzqeDLuradmzTDUuUBU+mV7l38zf58Z1v8/DGtzidvs07exm6Q4hHqEJzKrSrPfL+P7LY/5iH3X9CUTQXxAoOwZurDaZMWcppTcVJQlwBbAx1GZijKa8ejD6UzaCdz4qs7/9cofLzpTPnP3v+mmd24Tne0S/6zKvEhw2C13k5u5cSCE6IMsGrJyzVV4D94Cnq0DChSCYVj/fuBbVwVihthnevPnOKTmiHU7wfOJrd5t673+azm79Dk4UP7/+A5aRDrQbTKRNIDrc8wc1/jJt/zGTV4Jwj+Pq6Rt20kUGoUQH8yMg7riwLmNbQwxBefQzsDs8O7vOvC//iBJ6f1OcT18+/xjFw93w98guT8oUDyBdeJyLMUi2jwxztXPk5EDYJUiKmvR8AAAAASUVORK5CYII='
	});
});
