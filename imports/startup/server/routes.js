import * as ICS from 'ics-js';
ICS.VEVENT.validProps['X-APPLE-STRUCTURED-LOCATION'] = [];
Picker.route('/event/:eventId/ics', function (params, req, res, next) {
	// http://maxmouchet.github.io/ics.js/docs/ics.html
	// consider using https://github.com/sebbo2002/ical-generator if the current library is too limited
	// Use https://www.npmjs.com/package/ical VIA https://atmospherejs.com/meteorhacks/npm to Import

	var event = Event.findOne({_id: params.eventId});


	var calendar = new ICS.VCALENDAR();
	var iCalEvent = new ICS.VEVENT();
	calendar.addProp('VERSION', 2);
	calendar.addProp('PRODID', 'Pacfic Sky Event Calendar');
	iCalEvent.addProp('UID', 'pacificsky');
	iCalEvent.addProp('DTSTART', event.duration.start, {VALUE: 'DATE-TIME'});
	iCalEvent.addProp('DTEND', event.duration.end, {VALUE: 'DATE-TIME'});
	iCalEvent.addProp('SUMMARY', event.title);
	iCalEvent.addProp('DESCRIPTION', event.description);
	iCalEvent.addProp('URL', Meteor.absoluteUrl(`event/${event._id}`), {VALUE: 'URI'});
	if(event.location.lngLat) {
		iCalEvent.addProp('GEO', [event.location.lngLat.lat, event.location.lngLat.lng]);
		if(event.location.name) {
			iCalEvent.addProp(
				'X-APPLE-STRUCTURED-LOCATION',
				`geo:${event.location.lngLat.lat},${event.location.lngLat.lng}`,
				{
					VALUE: 'URI',
					'X-APPLE-RADIUS': 50.19423316229184,
					'X-TITLE': `"${event.location.name}\\n${event.location.street}, ${event.location.city}, ${event.location.state} ${event.location.zip}, ${event.location.country}"\n`
				}
			);
		} else {
			iCalEvent.addProp(
				'X-APPLE-STRUCTURED-LOCATION',
				`geo:${event.location.lngLat.lat},${event.location.lngLat.lng}`,
				{
					VALUE: 'URI',
					'X-APPLE-RADIUS': 50.19423316229184
				}
			);
		}
	}
	if(event.location.name) {
		iCalEvent.addProp('LOCATION', `${event.location.name}\\n${event.location.street}\\, ${event.location.city}\\, ${event.location.state} ${event.location.zip}\\, ${event.location.country}`);
	}
	iCalEvent.addProp('DTSTAMP', new Date(), {VALUE: 'DATE-TIME'});
	if(event.recurrence) {
		var recurrenceString = `FREQ=${DateFrequency.getIdentifiers()[event.recurrence.frequency]};INTERVAL=${event.recurrence.interval};`;
		if(event.recurrence.count) {
			recurrenceString += `COUNT=${event.recurrence.count};`;
		}
		recurrenceString += `UNTIL=${event.recurrence.dateRange.end.toISOString().replace(/-|:|\.\d\d\d/g, '').replace('Z', '')};`;
		// recurrenceString += `BYDAY=${moment(event.duration.start).format('dd').toUpperCase()};`;
		iCalEvent.addProp('RRULE', recurrenceString);
	}
	calendar.addComponent(iCalEvent);

	var icsString = calendar.toString();

	// var calendar = new iCalendar.CalendarBuilder();
	//
	//
	//
	// let iCalEvent = new iCalendar.EventBuilder();
	// iCalEvent.setStartDate( event.duration.start );
	// iCalEvent.setEndDate( event.duration.end );
	// iCalEvent.setSummary( event.description );
	// calendar.addEvent( iCalEvent.getEvent() );
	//
	// var icsString = calendar.getCalendar().toString();

	res.writeHead(200, {
		'Content-Type': 'text/calendar',
		'Content-Disposition': `attachment; filename="${event.title.substr(0, 20).toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')}.ics"`
	});
	res.end( icsString );
});
